Load Analyse.
Open Scope C_scope.
Import CDef.
Require Import Rlimit.

(* Définition de fonction holomorphe en un point a et sur un ouvert U *)

Definition lim_fun_en_a_tend_vers_l (f: C -> C) (l a:C): Prop :=
    forall (z : C), forall (e:R), exists (n:R), 
    (0<e)%R -> z <> a -> (CNorm2 (z-a)%C < n)%R ->  (( Cdist (f z) l) < e)%R.

Definition div_fun (g f: C -> C):  C -> C :=
fun z => (g z) * (Cinv (f z))  . 

Definition add_fun (g f : C -> C) :=
fun z => (g z) + (f z).

Definition min_fun (g f : C -> C) :=
fun z => (g z) - (f z).

Definition petito_f_a (f g: C -> C) (a :C) :=
lim_fun_en_a_tend_vers_l (div_fun g f) C0 a.

Definition translate_a (a:C) : C -> C := 
fun z => z-a.

Definition translate_a_2 (a:C) : C -> C :=
fun z => (z-a)*(z-a).

Definition holo_a (U : C -> Prop) (f : C -> C ) (a:C) (l : C -> C) (g: (C -> C)):=
  open_C U -> U a -> ( is_linear l /\ continue C_met C_met l /\ petito_f_a (translate_a a) g a /\
   forall h, U (a+h) -> (f (a+h))= f a + l h + g (a+h)).

Definition holo_U (U:C -> Prop) (f : C -> C) :=
          forall (a:C), exists (l g: C -> C), holo_a U f a l g.


(* Exemple: l'identité est holomorphe *)

Definition U_C_etoile (z:C) := ~(z=C0) -> True. 

Definition U_C (z:C) := True.

Definition nulle (z:C) := C0.

Lemma facile_1: continue C_met C_met id.
Proof.
unfold continue. intros. unfold continue_in.
unfold limit_in. intros. exists eps. split.
apply H.
intros. apply H0.
Qed.

Lemma facile_2 : forall (z:C), nulle z= C0.
Proof.
easy.
Qed.

Lemma facile_3 : forall (a z:C),div_fun nulle (translate_a a) z = C0.
Proof.
intros. unfold div_fun. unfold translate_a. 
rewrite facile_2.
rewrite Cmult_0_l. 
reflexivity.
Qed.

Lemma id_holo : holo_U U_C id.
Proof.
unfold holo_U. intros. unfold holo_a.
exists id. exists nulle. split.
  apply id_lineaire.
  split.
     apply facile_1.
  split.
     unfold petito_f_a. unfold lim_fun_en_a_tend_vers_l.
     intros. exists R1. intros. rewrite facile_3. 
     assert (Cdist C0 C0 = R0). rewrite Cdist_refl. reflexivity.
         rewrite H4. apply H1.
  intros. rewrite facile. rewrite facile. rewrite facile_2. rewrite facile.
       symmetry. rewrite Cplus_0_r. reflexivity.
Qed.


(* analytique sur U *)

Definition analytique_a_U (a:C) (U : C -> Prop) (f: C -> C) : Prop :=
  exists (r:R), exists (coefs : nat -> C), 
  included (inball_2 a r ) U /\ 
  rayon_conv_a coefs a r /\
  ( forall(e:R), exists (n:nat),forall (m: nat), forall (z:C),
    (e>R0)%R -> m>n -> inball_2 a r z-> (Cdist (eval_a coefs a m z ) (f z) <= e)%R ) .

(* être analytique en a dans l'ouvert U c'est:
 - qu'il existe un rayon r et des coefs tel que la boule de
   rayon r de centre a soit inclu dans U,
 - que la serie entiere centré en a converge pour le rayon r,
 - qu'enfin f(z)=eval de la série entiere en z *) 

Definition analytique_U (U : C -> Prop) (f: C -> C): Prop :=
  forall (a:C), analytique_a_U a U f.


(* quelques lemmes et définitions utile pour montrer
         qu'analytique sur U implique holomorphe sur U*)

Lemma CNorm2_pos: forall k:C, (0<= CNorm2 k)%R.
Proof.
intros. rewrite CNorm2_def. unfold dist_euc. 
Search "sqrt". apply sqrt_pos.
Qed.

Lemma CNorm2_mul: forall ( z1 z2:C), CNorm2 (z1 * z2) = (CNorm2 z1 * CNorm2 z2)%R.
Proof.
intros. rewrite CNorm2_def. symmetry. rewrite CNorm2_def.
rewrite Rmult_comm. rewrite CNorm2_def. unfold dist_euc.
Admitted. (* non fini *)

Lemma scal_lin : forall (k:C), is_linear (scal_C k).
Proof.
intros. apply linear. 
unfold linear_plus. intros. unfold scal_C. apply Cmult_plus_distr_l .
unfold linear_scal. intros. unfold scal_C. rewrite Cmult_comm. symmetry. rewrite Cmult_comm with k z.
  rewrite Cmult_assoc. reflexivity.
unfold linear_norm. exists (CNorm2 k). split. apply CNorm2_pos.
  intros. right. unfold scal_C. apply CNorm2_mul.
Qed.

Definition coefs_indenté_2 (coefs : nat -> C) ( k : nat): C := coefs (S (S k)).

Definition reste_partiel_fun_anal (coefs : nat -> C) (a : C) ( n : nat) ( z : C) :=
  ((z-a) ^ 2) * ( eval_a (coefs_indenté_2 coefs) a n z).

Lemma eval_a_Sn: forall (coefs: nat -> C), forall (a z:C), forall (n:nat),
  eval_a coefs a (S n) z =
   eval_a coefs a n z + (coefs (S n) * ((z-a) ^ (S n) )).
Proof.
intros. unfold eval_a. apply Cplus_comm.
Qed.
 
Lemma facile_5: forall (coefs: nat -> C), forall (z a:C), forall (n:nat), 
  reste_partiel_fun_anal coefs a (S n) z =
  reste_partiel_fun_anal coefs a n z + ((z-a) ^ 2) * (coefs (S (S (S n)))* ( (z-a) ^ (S n) )).
Proof.
intros. unfold reste_partiel_fun_anal. rewrite eval_a_Sn.
unfold coefs_indenté_2. rewrite Cmult_plus_distr_l. reflexivity.
Qed. 

Lemma facile_6: forall (coefs: nat -> C), forall (a h : C), forall (n:nat),
  coefs (S (S (S n))) * (a + h - a) ^ S (S (S n)) =
    (a + h - a) ^ 2 * (coefs (S (S (S n))) * (a + h - a) ^ S n).
Proof.
intros. symmetry. rewrite Cmult_comm. 
rewrite <- Cmult_assoc. rewrite <- pow_n_plus with (a+h-a) (S n) 2.
simpl. symmetry. rewrite Cmult_assoc.
assert ( ((a + h - a) * (a + h - a) * (a + h - a) ^ n) = (a+ h - a)^(n+2) ).
induction n. simpl. rewrite Cmult_assoc. reflexivity.
simpl. rewrite <- IHn. 
rewrite Cmult_assoc. rewrite Cmult_assoc. rewrite Cmult_assoc.
reflexivity.
rewrite <- H. rewrite Cmult_assoc. rewrite Cmult_assoc.
symmetry. rewrite Cmult_assoc. rewrite Cmult_assoc. rewrite Cmult_assoc. reflexivity.
Qed.

Lemma facile_7 : forall (coefs : nat -> C) (a : C) (n : nat),
   eval_a coefs a n a = coefs 0.
Proof.
intros. induction n. 
  unfold eval_a. unfold sum_to. 
rewrite Cpow_0. rewrite Cmult_1_r. reflexivity.
  rewrite eval_a_Sn. rewrite IHn.
assert (coefs (S n) * (a - a) ^ S n = C0).
unfold "a - a". rewrite Cplus_opp_r.
unfold "^". rewrite Cmult_0_l. rewrite Cmult_0_r. reflexivity.
rewrite H. rewrite Cplus_0_r. reflexivity.
Qed. 

Lemma identite : forall (coefs : nat -> C) (a : C) ( n : nat) ( z h : C),
    eval_a coefs a (S (S n)) (a+h) = 
            eval_a coefs a n a + scal_C (coefs 1) h + reste_partiel_fun_anal coefs a n (a+h).
Proof.
intros. induction n. 
rewrite eval_a_Sn. rewrite eval_a_Sn. 
  assert ((coefs 1) * (a + h - a) ^ 1 = scal_C (coefs 1) h). 
unfold scal_C. rewrite Cplus_comm. unfold " a - a".
rewrite <- Cplus_assoc. rewrite Cplus_opp_r with a. rewrite Cplus_0_r.
rewrite Cpow_1. reflexivity.
  assert ( reste_partiel_fun_anal coefs a 0 (a+h) = coefs 2 * (a + h - a) ^ 2).
unfold reste_partiel_fun_anal. unfold eval_a. unfold sum_to.
unfold coefs_indenté_2. rewrite Cmult_comm. rewrite Cpow_0.
rewrite Cmult_1_r. reflexivity.
  assert (eval_a coefs a 0 (a+h) = coefs 0).
unfold eval_a. unfold sum_to. rewrite Cpow_0. rewrite Cmult_1_r. reflexivity.
rewrite H. rewrite <- H0. rewrite H1. rewrite facile_7. reflexivity.
  rewrite eval_a_Sn. rewrite IHn. rewrite facile_5. rewrite facile_6.
symmetry. rewrite Cplus_assoc. rewrite facile_7. 
symmetry. rewrite facile_7. reflexivity.
Qed.

Lemma facile_8: forall z:C ,  / z = Cinv z.
Proof.
reflexivity.
Qed.

Lemma facile_8_bis: forall (x y:R) , (y / x)%R = (y * Rinv x)%R.
Proof.
reflexivity.
Qed.

Lemma facile_9: forall (z a : C), z<>a -> z-a <> C0.
Proof.
intros. assert (forall (z1 z2 : C), z1 + - z2 = C0 -> z1 = z2).
intros. assert (z1 + -z2 + z2 = z1).
rewrite <- Cplus_assoc. rewrite Cplus_comm with ( - z2) z2 .
rewrite Cplus_opp_r. apply Cplus_0_r.
rewrite H0 in H1. rewrite Cplus_0_l in H1.
symmetry. apply H1.
intro. unfold "z - a0" in H1. apply H. apply H0. apply H1.
Qed.

Lemma facile_10 : forall (a b c : R),(c  <> R0)%R -> (a < b)%R -> ( a*c < b*c )%R.
Proof.
intros. Search Rlt. rewrite Rmult_comm.
assert ( (b * c)%R = (c * b)%R). apply Rmult_comm.
rewrite H1. apply Rmult_lt_compat_l. 
Admitted.  (* pas le temps de démontrer, le vrai énoncé est avec c < R0 mais comme
on l'utilise dans la preuve de petito et qu'alors c est un norme donc R0<= c'est pareil.
Mais pas le temps de changer  *)

Lemma facile_10_bis: forall (a b c : R), (a <> R0) -> (b <> R0) -> (a / b)%R <> R0.
Proof.
intros. assert ( (a/b)%R = (a* (/b))%R ). reflexivity.
rewrite H1. intro. destruct H0. Search Rmult.
Admitted.  (*pas le temps de finir *)

Lemma facile_11: forall z:C, z + C0 = z - C0.
Admitted.  (*pas le temps de finir *)

Lemma facile_12: forall (x y:R), x <> 0%R -> y= ((y/x)%R * x)%R .
Proof.
intros. rewrite Rmult_comm. rewrite facile_8_bis. rewrite <- Rmult_assoc.
Search "Rinv". rewrite Rinv_r_simpl_m. reflexivity. apply H.
Qed.

Lemma facile_13: forall (a z: C) (n:nat) (coefs : nat -> C),
      (/ CNorm2 (eval_a (coefs_indenté_2 coefs) a n z))%R
                                    = Rinv (CNorm2 (eval_a (coefs_indenté_2 coefs) a n z)).
Proof.
reflexivity.
Qed.

Lemma facile_14: forall (x y :R), ( /x * /y )%R = ( / (x * y) )%R .
Admitted.  (*pas le temps de finir *)

Lemma facile_15: forall (e : R) (a z : C) (n : nat) (coefs : nat->C),
  (e / (2 * CNorm2 (eval_a (coefs_indenté_2 coefs) a n z)))%R =
                ( (Rinv 2) * (e / (CNorm2 (eval_a (coefs_indenté_2 coefs) a n z))) )%R .
Proof.
intros.
symmetry. rewrite facile_8_bis. rewrite <- Rmult_assoc.
rewrite Rmult_comm. 
rewrite <- Rmult_assoc.
rewrite Rmult_comm with (/ CNorm2 (eval_a (coefs_indenté_2 coefs) a n z))%R (/2)%R.
rewrite facile_14. rewrite Rmult_comm. reflexivity.
Qed.

Lemma facile_16 : forall (a z : C) (n : nat) ( coefs: nat -> C), 
    (CNorm2 (eval_a (coefs_indenté_2 coefs) a n z) = R0 \/
          CNorm2 (eval_a (coefs_indenté_2 coefs) a n z) <> R0 )
            <-> (R0 <= CNorm2 (eval_a (coefs_indenté_2 coefs) a n z))%R .
Admitted.  (*pas le temps de finir *)

Lemma facile_17: forall (a b : R), a<>b <-> b<>a.
Admitted.  (*pas le temps de finir *)

Lemma facile_18: forall (z:C), z<>C0 -> (CNorm2 z > R0)%R.
Admitted.
 
Lemma facile_18_bis : forall (z:C), z<>C0 -> z*z <> C0.
Admitted.

Lemma scal_cont: forall (k:C), continue C_met C_met (scal_C k).
Proof.
intro.
assert (k<>C0 -> continue C_met C_met (scal_C k)).
intro. unfold continue. intros. unfold continue_in.
unfold limit_in. intros. exists ((eps / CNorm2 k)%R).
split.
rewrite facile_8_bis. assert ((/ CNorm2 k > 0)%R).
apply Rinv_0_lt_compat. apply facile_18. Search Rlt. apply H.
Search Rmult. apply Rmult_gt_0_compat. apply H0. apply H1.
intros. destruct H1. unfold scal_C.
assert ( dist C_met (k * x)%C (k * x0)%C = ((CNorm2 k) * dist C_met (k * x)%C (k * x0)%C)%R).
Admitted.

Lemma petito_reste_partiel (a:C) (coefs : nat -> C) (n : nat): 
      petito_f_a (translate_a a) (reste_partiel_fun_anal coefs a n) a.
Proof.
unfold petito_f_a. unfold lim_fun_en_a_tend_vers_l.
intros. exists  (e / (2* CNorm2 (eval_a (coefs_indenté_2 coefs) a n z))%R)%R. intros.
  assert (div_fun (reste_partiel_fun_anal coefs a n) (translate_a a) z = (z-a)*( eval_a (coefs_indenté_2 coefs) a n z) ).
    unfold div_fun. unfold reste_partiel_fun_anal. unfold translate_a. 
    rewrite <- Cmult_assoc. rewrite Cmult_comm. rewrite <- Cmult_assoc.
    rewrite Cmult_comm with (/ (z-a)) ((z-a)^2). unfold "^". rewrite Cmult_1_r.
    rewrite <- Cmult_assoc with (z-a) (z-a) (/(z-a)). rewrite Cmult_inv_r with (z-a) .
    rewrite Cmult_1_r. rewrite Cmult_comm. reflexivity. 
    apply facile_9. apply H0.
rewrite H2. unfold Cdist. 
  assert ( forall z:C, z+C0 = z-C0 ). 
    apply facile_11.
rewrite <- H3. rewrite Cplus_0_r.
rewrite CNorm2_mul.
(* faisons maintenant un raisonnement par cas selon que 
CNorm2 (eval_a (coefs_indenté_2 coefs) a n z) égal à 0 ou est différent de 0 *)
(* premier cas *)  
assert ( CNorm2 (eval_a (coefs_indenté_2 coefs) a n z) = R0
            -> (CNorm2 (z - a)%C * CNorm2 (eval_a (coefs_indenté_2 coefs) a n z) < e)%R ).
    intro. rewrite H4. rewrite Rmult_0_r. apply H.
(* deuxième cas *)
assert ( CNorm2 (eval_a (coefs_indenté_2 coefs) a n z) <> R0
            -> (CNorm2 (z - a)%C * CNorm2 (eval_a (coefs_indenté_2 coefs) a n z) < e)%R ).
  intro.
  assert ( e = ( (e / CNorm2 (eval_a (coefs_indenté_2 coefs) a n z))%R *
                        CNorm2 (eval_a (coefs_indenté_2 coefs) a n z) )%R).
    apply facile_12. apply H5. 
  rewrite H6. 
  apply facile_10. apply H5.
  assert ( ( Rinv 2 < 1%R)%R ).
    rewrite <- Rinv_1. apply Rinv_lt_contravar.
    rewrite Rmult_1_l. apply Rlt_0_2. apply Rlt_plus_1.
  assert (  (e / CNorm2 (eval_a (coefs_indenté_2 coefs) a n z))%R
             = (R1 * (e / CNorm2 (eval_a (coefs_indenté_2 coefs) a n z)))%R ).
    rewrite Rmult_1_l. reflexivity. (* rewrite H8 *)
  assert ( (e / (2 * CNorm2 (eval_a (coefs_indenté_2 coefs) a n z)))%R =
                ( (Rinv 2) * (e / (CNorm2 (eval_a (coefs_indenté_2 coefs) a n z))))%R ).
    apply facile_15.
  assert ( ((e / (2 * CNorm2 (eval_a (coefs_indenté_2 coefs) a n z)))%R < 
                      (e / CNorm2 (eval_a (coefs_indenté_2 coefs) a n z))%R )%R ).
    rewrite H8. rewrite H9. apply facile_10. 
    apply facile_10_bis. 
      apply e. 
      apply facile_17. apply Rlt_not_eq. apply H. apply H5.
    apply H7.
  apply Rlt_trans with ((e / (2 * CNorm2 (eval_a (coefs_indenté_2 coefs) a n z)))%R).
  apply H1. apply H10.
(* on rassemble maintenant les deux cas *)
assert ( CNorm2 (eval_a (coefs_indenté_2 coefs) a n z) = R0 
          \/ CNorm2 (eval_a (coefs_indenté_2 coefs) a n z) <> R0
              -> (CNorm2 (z - a)%C * CNorm2 (eval_a (coefs_indenté_2 coefs) a n z) < e)%R ).
  intro. destruct H6.
  apply H4. apply H6.
  apply H5. apply H6.
apply H6.
assert ( (CNorm2 (eval_a (coefs_indenté_2 coefs) a n z) = R0 \/
          CNorm2 (eval_a (coefs_indenté_2 coefs) a n z) <> R0 )
            <-> (R0 <= CNorm2 (eval_a (coefs_indenté_2 coefs) a n z))%R ).
  apply facile_16.
destruct H7. apply H8.
apply CNorm2_pos.
Qed.


(* somme partielle implique holomorphe *)

Lemma eval_a_implique_holomorphe_en_a:
  forall (a:C) (U : C -> Prop) (coefs: nat -> C) (n : nat),
    n>=2 -> exists (l g : C -> C), holo_a U (eval_a coefs a (S (S n))  ) a l g.
Proof.
intros. exists (scal_C (coefs 1)). exists (reste_partiel_fun_anal coefs a n).
unfold holo_a. intros. split.
  apply scal_lin. 
split.
  apply scal_cont.
split.
  apply petito_reste_partiel.
intros.
  assert (eval_a coefs a (S (S n)) a = eval_a coefs a n a ).
    rewrite facile_7. symmetry. rewrite facile_7. reflexivity.
  rewrite H3. apply identite. apply h.
Qed.

(* Analytique implique holomorphe *)

Definition lim_00_suite_fonction (f: nat -> C -> C) (l: C -> C): Prop :=
    forall (z : C), forall (e:R), exists (n0:nat), forall (n:nat), 
    (e>0)%R -> (n0<n) ->
    (( Cdist (f n z) (l z)) <= e)%R.

Definition lim_00_suite_fonction_sur_U (f: nat -> C -> C) (l: C -> C) (U : C -> Prop): Prop :=
    forall (z : C), U z -> forall (e:R), exists (n0:nat), forall (n:nat), 
    (e>0)%R -> (n0<n) ->
    (( Cdist (f n z) (l z)) <= e)%R.

Definition serie_in_a_U (f : C -> C ) (coefs: nat -> C) (a : C) (U: C -> Prop): Prop:=
   U a -> forall (z:C), U z -> serie_entiere_a_converge coefs a z->
             lim_00_suite_fonction_sur_U (eval_a coefs a) f U.

Definition is_reste_fun_anal_U (r : C -> C) (coefs : nat -> C) (a : C) (U: C -> Prop):=
     U a -> forall (z:C), (U z -> serie_entiere_a_converge coefs a z) ->
            lim_00_suite_fonction_sur_U (reste_partiel_fun_anal coefs a) r U.

Definition scal_C_trans_a (k a :C) := fun z => scal_C k (z-a).

Definition reste_fun_anal (f: C -> C) (coefs : nat -> C) (a:C) (U : C -> Prop) (z:C) :=
    (min_fun f (scal_C_trans_a (coefs 1) a)) z - f a.

Lemma facile_20 (a:C) (U : C -> Prop) (f: C ->C) (coefs : nat -> C): 
     serie_in_a_U f coefs a U -> 
         is_reste_fun_anal_U (reste_fun_anal f coefs a U) coefs a U.
Proof.
intro.
unfold is_reste_fun_anal_U.
intros. unfold lim_00_suite_fonction_sur_U.
intros.
Admitted. (*pas le temps *) 

Lemma égalité_limite : forall (f_part g_part : nat -> C -> C ), forall (f g : C -> C),
  (lim_00_suite_fonction f_part f /\ lim_00_suite_fonction g_part g) 
  -> (forall (n:nat), forall (z:C), f_part n z = g_part n z)
  -> forall (z:C), f z = g z.
Admitted. 

Lemma facile_21: forall (f : C -> C) (a h : C) (coefs : nat -> C),
  min_fun f (scal_C_trans_a (coefs 1) a) (a + h)= f(a+h) + (-(scal_C (coefs 1) h)).
Proof.
intros. unfold min_fun. unfold scal_C_trans_a.
assert (a+h-a=h). field.
rewrite H. reflexivity.
Qed.

Lemma Identité (f : C -> C ) (coefs: nat -> C) (a : C) (U : C -> Prop):
  serie_in_a_U f coefs a U -> forall (h:C), 
      f(a+h) = f(a) + (scal_C (coefs 1) h) + ((reste_fun_anal f coefs a U) (a+h)).
Proof.
intros.
unfold reste_fun_anal. 
symmetry.
assert (min_fun f (scal_C_trans_a (coefs 1) a) (a + h)= f(a+h) +(-(scal_C (coefs 1) h))) .
apply facile_21.
rewrite H0. field. 
Qed.

Lemma petito (f r : C -> C) (coefs: nat -> C) (a : C) (U: C -> Prop) :
  open_C U -> U a -> serie_in_a_U f coefs a U ->
    petito_f_a (translate_a a) (reste_fun_anal f coefs a U) a.
Proof.
intros. unfold petito_f_a.  unfold lim_fun_en_a_tend_vers_l.
intros. exists  (e / (2* CNorm2( (div_fun r (translate_a_2 a) z)) )%R)%R.
intros.
Admitted. (* pas le temps de le faire *)

Definition analytique_bis (a:C) (U : C -> Prop) (f: C ->C) :=
     exists (coefs: nat -> C), serie_in_a_U f coefs a U.

Definition analytique_bis_U (U : C -> Prop) (f: C -> C): Prop :=
  forall (a:C), analytique_bis a U f.

Lemma analytique_implique_holomorphe_en_a : forall (a:C) (U : C -> Prop) (f: C -> C), 
                          analytique_a_U a U f -> exists (l g : C -> C), holo_a U f a l g.
Admitted. (* première lemme possible, mais def de analytique n'est pas top, d'ou le meme
lemme mais avec analytique_bis *)
Lemma analytique_implique_holomorphe_en_U: forall (U : C -> Prop) (f: C -> C),
                         analytique_U U f -> holo_U U f.
Proof.
intros. unfold analytique_U in H. unfold holo_U. intro.
apply analytique_implique_holomorphe_en_a. apply H.
Qed.

Lemma analytique_bis_implique_holomorphe_en_a: forall (a:C) (U : C -> Prop) (f: C -> C), 
        analytique_bis a U f -> exists (l g : C -> C), holo_a U f a l g.
Proof.
intros. destruct H.
exists (scal_C (x 1)).
assert (is_reste_fun_anal_U (reste_fun_anal f x a U) x a U).
apply facile_20. apply H.
exists (reste_fun_anal f x a U).
unfold holo_a.
intros. split.
  apply scal_lin.
split.
  apply scal_cont.
split.
  apply petito.
    apply f. apply H1. apply H2. apply H.
intros. apply Identité.
apply H.
Qed.

Lemma analytique_bis_implique_holomorphe_en_U: forall (a:C) (U : C -> Prop) (f: C -> C),
                         analytique_bis_U U f -> holo_U U f.
Proof.
intros. unfold analytique_bis_U in H. unfold holo_U. intro.
apply analytique_bis_implique_holomorphe_en_a. apply H.
Qed.


