Load Cdefs.
Import CDef.
Open Scope C_scope.

Axiom func_ext_dep : forall (A : Type) (B : A -> Type) (f g : forall x, B x),
  (forall x, f x = g x) -> f = g.

Definition comp {A B C : Type} (f : A -> B) (g : B -> C) := (fun a => g (f a)).
Notation "f ° g" := (comp g f) (at level 80).
Definition comp2 {A B C D : Type} (f : A -> B) (g : A -> C) (h : B -> C -> D) :=
  fun a => h (f a) (g a).

Notation "h << f | g" := (comp2 f g h) (at level 80).
(* Définition d'une fonction continue entre deux espaces métriques *)

Definition D_x (E : Type) (D: E -> Prop) (y x:E) : Prop := D x /\ y <> x.

Definition continue_at (E F : Metric_Space) (f : Base E -> Base F)
           (D : Base E -> Prop) (x0 : Base E) : Prop :=
  limit_in E F f (D_x (Base E) D x0) x0 (f x0).

Definition continue_in (E F : Metric_Space) (f : Base E -> Base F)
           (D : Base E -> Prop) : Prop :=
  forall x0 : Base E, (D x0) -> continue_at E F f D x0.
Definition continue (E F : Metric_Space) (f : Base E -> Base F) :=
  forall (x0 : Base E), continue_at E F f (fun x => True) x0.


(* Series entières *)
Fixpoint sum_to (expr : nat -> C) (n : nat) :=
  match n with
  |0 => expr 0
  |S m => expr n + sum_to expr m
  end.
Check Cpow.

Definition eval (coefs : nat -> C) (z : C) (n : nat) :=
  sum_to (fun k => (coefs k) * (Cpow z k)) n.

Definition eval_a  (coefs : nat -> C) (a : C) (n : nat) (z : C) :=
   sum_to (fun k => (coefs k) * (Cpow (z-a) k)) n.

Definition serie_entiere_0_converge (coefs : nat -> C) (z : C) : Prop :=
  forall epsilon : R, (R0 < epsilon)%R -> exists N : nat, forall (m n : nat),
        (Cdist (eval coefs z m) (eval coefs z n) <= epsilon)%R.

Definition serie_entiere_a_converge (coefs : nat -> C) (a : C) (z : C) : Prop :=
  forall epsilon : R, (R0 < epsilon)%R -> exists N : nat, forall (m n : nat),
        (Cdist (eval_a coefs a m z) (eval_a coefs a n z) <= epsilon)%R.

Definition INC : nat -> C := R_to_C ° INR.
Definition SE_plus (s1 s2 : nat -> C) := Cplus << s1 | s2.
Definition SE_mult (s1 s2 : nat -> C) :=
  sum_to (fun n => (Cmult << s1 | (s2 ° (fun k => minus n k))) n).
Definition SE_derive (s : nat -> C) := (Cmult << INC | s) ° Nat.succ.

(*Boule ouverte, voisinnage, ouvert, rayon de convergence de serie entiere. *)

Definition included (D1 D2:C -> Prop) : Prop := forall x:C, D1 x -> D2 x.

Definition inball_2 (a:C) (r:R) (z:C) : Prop := (Cdist z a < r)%R.

Definition neighbourhood_a (V:C -> Prop) (a:C) : Prop :=
  exists r : R, included (inball_2 a r) V.

Definition open_C (D:C -> Prop) : Prop :=
  forall x:C, D x -> neighbourhood_a D x.

Definition rayon_conv_a (coefs : nat -> C) (a: C) (r : R) : Prop :=
  (r > R0)%R -> forall (z:C), inball_2 a r z -> serie_entiere_a_converge coefs a z. 





(* Fonction scalaire, Fonction partiel, fonction lineaire *)

Definition scal_C (k:C) (z:C) : C := k * z.

Definition part_fun (U V : C -> Prop) (f: C -> C) := forall x, U x -> V (f x).

Definition linear_plus (l: C-> C) :Prop :=
           forall (z z' : C), l ( z + z') = (l z) + (l z').
Definition linear_scal (l: C-> C): Prop :=
           forall (k : C) (z : C), l (scal_C k z) = scal_C k (l z).
Definition linear_norm (l :C -> C) : Prop :=
           exists M : R, (0 <= M)%R /\
                                (forall z : C, (CNorm2 (l z) < M * CNorm2 z )%R
                                               \/ (CNorm2 (l z) = M * CNorm2 z )%R).

Inductive is_linear (l : C -> C) : Prop :=
          |linear : linear_plus l -> linear_scal l -> linear_norm l -> is_linear l.

Record is_linear_bis (l: C-> C) :=    (*autre definition possible d'être linéaire *) 
    {
    linear_plus_bis : forall (z z' : C), l ( z + z') = (l z) + (l z') ;
    linear_scal_bis : forall (k : C) (z : C), l (scal_C k z) = scal_C k (l z) ;
    linear_norm_bis : exists M : R, (0 <= M)%R /\
                                (forall z : C, (CNorm2 (l z) < M * CNorm2 z )%R
                                               \/ (CNorm2 (l z) = M * CNorm2 z )%R)}.


(* exemple de fonction linéaire *)

Definition id (z: C) : C := z.

Lemma facile: forall z:C, id z = z.
Proof.
easy.
Qed.

Lemma id_lineaire : is_linear id.
Proof.
apply linear.
easy.
easy.
exists R1. split. apply Rle_0_1.
intros. right. 
rewrite facile.
rewrite Rmult_1_l. reflexivity.
Qed.

(*les deux définition de linéaire sont équivalente*)
Axiom lin:  forall (l: C -> C), is_linear l -> is_linear_bis l. (* je ne sais pas le démontrer,
                                                                 je ne sais pas casser le record quand il est dans le but*)

Lemma equiv_lin : forall (l: C -> C), is_linear_bis l <-> is_linear l.
Proof.
intros. split.
intros. destruct H. apply linear.
unfold linear_plus. apply linear_plus_bis0.
unfold linear_scal. apply linear_scal_bis0.
unfold linear_norm. apply linear_norm_bis0.
apply lin. 
Qed.

(* Deux définitions d'appli lineaire restreinte à U *)

Definition linear_plus_ext (U : C -> Prop) (l : C -> C): Prop := 
           forall (z z' : C), U z -> U z' -> l(z + z') = (l z) + (l z').
Definition linear_scal_ext (U : C -> Prop) (l : C -> C): Prop :=
           forall (k : C) (z : C), U z -> l (scal_C k z) = scal_C k (l z).
Definition linear_norm_ext (U : C -> Prop) (l : C -> C): Prop :=
           exists M : R, (0 <= M)%R /\
                                (forall z : C, U z -> ((CNorm2 (l z) < M * CNorm2 z )%R
                                               \/ (CNorm2 (l z) = M * CNorm2 z )%R)).

Inductive is_linear_ext (U : C -> Prop) (l : C -> C): Prop := 
  |linear_ext : linear_plus_ext U l -> linear_scal_ext U l ->
             linear_norm_ext U l -> is_linear_ext U l.

Record is_linear_ext_bis (U : C -> Prop) (l : C -> C) := {
    linear_plus_ext_bis :  forall (z z' : C), U z -> U z' -> l(z + z') = (l z) + (l z'); 
    linear_scal_ext_bis :  forall (k : C) (z : C), U z -> l (scal_C k z) = scal_C k (l z) ;
    linear_norm_ext_bis :  exists M : R, (0 <= M)%R /\
                                (forall z : C, U z -> ((CNorm2 (l z) < M * CNorm2 z )%R
                                               \/ (CNorm2 (l z) = M * CNorm2 z )%R))}.

Axiom lin_ext : forall (U: C -> Prop) (l : C -> C),
                is_linear_ext U l -> is_linear_ext_bis U l.

Lemma equiv_lin_ext: forall (U : C -> Prop) (l : C -> C),
                is_linear_ext U l <-> is_linear_ext_bis U l.
Proof.
intros. split. apply lin_ext.
intro. destruct H. apply linear_ext.
unfold linear_plus_ext. apply linear_plus_ext_bis0.
unfold linear_scal_ext. apply linear_scal_ext_bis0.
unfold linear_norm_ext. apply linear_norm_ext_bis0. 
Qed.


Lemma linear_restrict: forall (U : C -> Prop) (l : C -> C), 
                    is_linear l -> is_linear_ext U l.
Proof.
intros. destruct H. apply linear_ext.
unfold linear_plus_ext in *. intros. apply H.
unfold linear_scal_ext in *. intros. apply H0.
unfold linear_norm_ext in *. intros. destruct H1. exists x. split. apply H1.
intros. apply H1.
Qed.


(* def de lineaire entre U et V , et exemple pour identité sur boule centré en 0 de rayon 1 *)

Definition linear_U_V (U V :C -> Prop) (l: C -> C):= is_linear_ext U l /\ part_fun U V l.  (* inutile pour la suite *)

Definition boule_centré_en_0_de_rayon_1 (z:C) :Prop := inball_2 C0 R1 z.

Lemma id_boule_lineaire: linear_U_V boule_centré_en_0_de_rayon_1 boule_centré_en_0_de_rayon_1 id.
Proof.
unfold linear_U_V in *. split. apply linear_restrict. apply id_lineaire.
easy.
Qed.


