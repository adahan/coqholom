Require Import Reals Arith List.
Require Import Psatz.
Import ListNotations.
Delimit Scope C_scope with C.

(* Construction de C*)

Local Open Scope C_scope.
Module CDef.
  Parameter C : Set.
  Axiom R_to_C : R -> C.
  Axiom Im : C -> R. (* Those two lines might be unneccesary *)
  Axiom Re : C -> R.

  Axiom CInj : forall (z1 z2 : C), Im z1 = Im z2 -> Re z1 = Re z2 -> z1 = z2.
  Axiom SurjImRe : forall (a b : R), exists (c : C), Re c = a /\ Im c = b.
  Axiom RInj : forall (x : R), Re (R_to_C x) = x.
  Axiom RtoC_noIm : forall (x : R), Im (R_to_C x) = R0.

  Parameter C0 : C.
  Parameter C1 : C.
  Parameter Ci : C.

  Parameter Cplus : C -> C -> C.
  Parameter Cmult : C -> C -> C.
  Parameter Copp : C -> C.
  Parameter Cinv : C -> C.
  Parameter CNorm2 : C -> R.

  Parameter C0_def : C0 = R_to_C R0.
  Parameter C1_def : C1 = R_to_C R1.
  Parameter Ci_def_Im : Im Ci = R1.
  Parameter Ci_def_Re : Re Ci = R0.

  Parameter Cplus_def_Im : forall (z1 z2 : C), Im (Cplus z1 z2) = ((Im z1) + (Im z2))%R.
  Parameter Cplus_def_Re : forall (z1 z2 : C), Re (Cplus z1 z2) = ((Re z1) + (Re z2))%R.


  Parameter Cmult_def_Im : forall (z1 z2 : C), Im (Cmult z1 z2) =
                                               (((Im z1) * (Re z2)) + ((Im z2) * (Re z1)))%R.
  Parameter Cmult_def_Re : forall (z1 z2 : C), Re (Cmult z1 z2) =
                                               (((Re z1) * (Re z2)) - ((Im z1) * (Im z2)))%R.

  Parameter Copp_def_Im : forall z : C, Im (Copp z) = Ropp (Im z).
  Parameter Copp_def_Re : forall z : C, Re (Copp z) = Ropp (Re z).

  Parameter Cinv_RtoC : forall r : R, Cinv (R_to_C r) = R_to_C (Rinv r).
  Parameter Cmult_inv_r : forall z : C, z <> C0 -> Cmult z (Cinv z) = C1.
  Parameter CNorm2_def : forall z : C, CNorm2 z = dist_euc 0 0 (Re z) (Im z).

 
(* Quelques lemmes utile sur C *)

  Fixpoint Cpow (z : C) (n : nat) := match n with
                                    |O => C1
                                    |S n => Cmult z (Cpow z n)
                                    end.

  Infix "+" := Cplus : C_scope.
  Infix "*" := Cmult : C_scope.
  Infix "^" := Cpow : C_scope.
  Notation "/ x" := (Cinv x) : C_scope.
  Notation "- x" := (Copp x) : C_scope.


  Definition Cminus (z1 z2 : C) : C := z1 + - z2.
  Infix "-" := Cminus : C_scope.

  Definition Cdiv (z1 z2 : C) := z1 * (Cinv z2).
  Infix "/" := Cdiv : C_scope.

  Lemma Cplus_0_r : forall (z : C), z + C0 = z.
  Proof.
    intro z.
    apply CInj.
    * rewrite Cplus_def_Im ; rewrite C0_def ; rewrite RtoC_noIm.
      field.
    * rewrite Cplus_def_Re ; rewrite C0_def ; rewrite RInj.
      field.
  Qed.


  Lemma Cplus_opp_r : forall (z : C), z + - z = C0.
  Proof.
    intro z.
    apply CInj.
    * rewrite Cplus_def_Im ; rewrite Copp_def_Im ; rewrite C0_def ; rewrite RtoC_noIm.
      field.
    * rewrite Cplus_def_Re ; rewrite Copp_def_Re ; rewrite C0_def ; rewrite RInj.
      field.
  Qed.

  Lemma Cplus_comm : forall (z1 z2 : C), Cplus z1 z2 = Cplus z2  z1.
  Proof.
    intros z1 z2.
    apply (CInj (Cplus z1 z2)).
    * rewrite (Cplus_def_Im z1 z2).
      rewrite (Cplus_def_Im z2 z1).
      rewrite (Rplus_comm).
      reflexivity.
    * rewrite (Cplus_def_Re z1 z2).
      rewrite (Cplus_def_Re z2 z1).
      rewrite (Rplus_comm).
      reflexivity.
  Qed.

  Lemma Cplus_0_l : forall (z : C), C0 + z = z.
  Proof.
    intro. rewrite Cplus_comm. exact (Cplus_0_r z).
  Qed.

  Lemma Cplus_assoc : forall (z1 z2 z3 : C), z1 + (z2 + z3) = (z1 + z2) + z3.
  Proof.
    intros z1 z2 z3.
    apply CInj.
    * rewrite Cplus_def_Im ;rewrite Cplus_def_Im ;rewrite Cplus_def_Im ;rewrite Cplus_def_Im .
      rewrite (Rplus_assoc).
      reflexivity.
    * rewrite Cplus_def_Re; rewrite Cplus_def_Re; rewrite Cplus_def_Re; rewrite Cplus_def_Re.
      rewrite (Rplus_assoc).
      reflexivity.
  Qed.

  Lemma Cmult_1_r : forall (z : C), z * C1 = z.
  Proof.
    intro z.
    apply (CInj (z * C1)).
    * rewrite (Cmult_def_Im).
      rewrite (C1_def).
      rewrite RInj ; rewrite RtoC_noIm.
      rewrite Rmult_0_l ; rewrite Rmult_1_r ; rewrite Rplus_0_r.
      reflexivity.
    * rewrite Cmult_def_Re.
      rewrite C1_def.
      rewrite RInj ; rewrite RtoC_noIm.
      rewrite Rmult_1_r ; rewrite Rmult_0_r.
      unfold Rminus.
      assert (- 0 = 0)%R.
      rewrite<- (Rplus_0_l (- 0)).
      rewrite Rplus_opp_r ; reflexivity.
      rewrite H ; rewrite Rplus_0_r ; reflexivity.
  Qed.
  
   Lemma Cmult_0_l: forall z:C, C0*z=C0.
   Proof.
    intros. apply (CInj (C0 * z)).
    rewrite (Cmult_def_Im).
      rewrite (C0_def).
      rewrite RInj ; rewrite RtoC_noIm.
      rewrite Rmult_0_l; rewrite Rmult_0_r ; rewrite Rplus_0_r.
      reflexivity.
    rewrite Cmult_def_Re.
      rewrite C0_def.
      rewrite RInj ; rewrite RtoC_noIm.
      rewrite Rmult_0_l ; rewrite Rmult_0_l.
      unfold Rminus.
      assert (- 0 = 0)%R.
      rewrite<- (Rplus_0_l (- 0)).
      rewrite Rplus_opp_r ; reflexivity.
      rewrite H ; rewrite Rplus_0_r ; reflexivity.
 Qed.  

  Lemma Cmult_assoc : forall (z1 z2 z3 : C), z1 * (z2 * z3) = (z1 * z2) * z3.
  Proof.
    intros z1 z2 z3.
    apply CInj.
    * rewrite Cmult_def_Im ; rewrite Cmult_def_Im ; rewrite Cmult_def_Re.
      rewrite Cmult_def_Im ; rewrite Cmult_def_Im ; rewrite Cmult_def_Re.
      field.
    * rewrite Cmult_def_Re ; rewrite Cmult_def_Re ; rewrite Cmult_def_Im.
      rewrite Cmult_def_Re ; rewrite Cmult_def_Re ; rewrite Cmult_def_Im.
      field.
  Qed.

  Lemma Cmult_comm : forall (z1 z2 : C), z1 * z2 = z2 * z1.
  Proof.
    intros z1 z2.
    apply CInj.
    * rewrite Cmult_def_Im ; rewrite Cmult_def_Im.
      field.
    * rewrite Cmult_def_Re ; rewrite Cmult_def_Re.
      field.
  Qed.

  Lemma Cmult_0_r: forall z:C, z * C0 = C0.
  Proof.
    intros. rewrite Cmult_comm. rewrite Cmult_0_l. reflexivity.
  Qed.

  Lemma Cmult_1_l : forall z : C, C1 * z = z.
  Proof.
    intro. rewrite Cmult_comm. exact (Cmult_1_r z).
  Qed.

  Lemma Cmult_inv_l : forall z : C, z <> C0 -> /z * z = C1.
  Proof.
    intros.
    rewrite Cmult_comm.
    exact (Cmult_inv_r z H).
  Qed.
  Search Rmult.
  
  Lemma Cmult_plus_distr_r : forall (z1 z2 z3 : C), (z1 + z2) * z3 = z1 * z3 + z2 * z3.
  Proof.
    intros.
    apply CInj.
    * rewrite Cmult_def_Im.
      rewrite ! Cplus_def_Im.
      rewrite ! Cmult_def_Im.
      rewrite Cplus_def_Re.
      field.
    * rewrite Cplus_def_Re.
      rewrite ! Cmult_def_Re.
      rewrite Cplus_def_Im.
      rewrite Cplus_def_Re.
      field.
  Qed.

  Lemma Cmult_plus_distr_l : forall (z1 z2 z3 : C), z3 * (z1 + z2) = z3 * z1 + z3 * z2.
Proof.
intros. rewrite Cmult_comm. symmetry. 
rewrite Cmult_comm. rewrite Cplus_comm. rewrite Cmult_comm. rewrite Cplus_comm.
symmetry. apply Cmult_plus_distr_r.
Qed.
  Search Copp.

  Lemma Cminus_def : forall z1 z2 : C, z1 - z2 = z1 + - z2.
  Proof.
    intros.
    unfold Cminus.
    reflexivity.
  Qed.

  Definition ring_comp := mk_rt C0 C1 Cplus Cmult Cminus Copp eq Cplus_0_l Cplus_comm
                                       Cplus_assoc Cmult_1_l Cmult_comm Cmult_assoc
                                       Cmult_plus_distr_r (fun x y => eq_refl) Cplus_opp_r.

  Lemma C1_neq_C0 : C1 <> C0.
  Proof.
    intro.
    assert (Re C1 = Re C0).
    rewrite H.
    reflexivity.
    rewrite C0_def in H0.
    rewrite C1_def in H0.
    rewrite ! RInj in H0.
    exact (R1_neq_R0 H0).
  Qed.



  Definition field_comp := mk_field Cdiv Cinv ring_comp C1_neq_C0 (fun x y => eq_refl)
                                    Cmult_inv_l.

  Lemma pow_n_plus : forall (z : C) (m n : nat), z ^ (m + n) = (z ^ m) * (z ^ n).
  Proof.
    intros z m n.
    induction n.
    * simpl.
      rewrite Cmult_1_r.
      rewrite Nat.add_0_r.
      reflexivity.
    * rewrite Nat.add_succ_r.
      simpl.
      rewrite Cmult_assoc.
      rewrite (Cmult_comm (z^m) z).
      rewrite IHn, Cmult_assoc.
      reflexivity.
  Qed.

  Lemma Cpow_0 : forall (z : C), z ^ 0 = C1.
  Proof.
    intros. unfold "^". reflexivity.
  Qed.

Lemma Cpow_1 : forall (z : C), z ^ 1 = z.
  Proof.
    intros. unfold "^". rewrite Cmult_1_r. reflexivity.
  Qed.

  Lemma C_power_theory : power_theory C1 Cmult eq N.to_nat Cpow.
  Proof.
    constructor. destruct n. reflexivity.
    simpl. induction p.
    - rewrite Pos2Nat.inj_xI. simpl. now rewrite plus_0_r, pow_n_plus, IHp.
    - rewrite Pos2Nat.inj_xO. simpl. now rewrite plus_0_r, pow_n_plus, IHp.
    - simpl. rewrite Cmult_comm; apply Cmult_1_l.
  Qed.

  Ltac Cpow_tac t :=
  match isnatcst t with
  | false => constr:(InitialRing.NotConstant)
  | _ => constr:(N.of_nat t)
  end.
  Add Field complexes : field_comp (power_tac C_power_theory [Cpow_tac]).

  Definition Cdist (z1 z2 : C) : R := CNorm2 (z1 - z2).

  Lemma Cdist_eq_euc_sqr :
    forall (z1 z2 : C), Cdist z1 z2 = dist_euc (Re z1) (Im z1) (Re z2) (Im z2).
  Proof.
    intros z1 z2.
    unfold Cdist ; rewrite CNorm2_def.
    unfold Cminus.
    repeat (rewrite Cplus_def_Im ; rewrite Cplus_def_Re
            ; rewrite Copp_def_Im ; rewrite Copp_def_Re).
    unfold dist_euc.
    repeat (rewrite Rminus_0_l).
    repeat (rewrite <- Rsqr_neg).
    reflexivity.
  Qed.
  
  Lemma Cdist_refl : forall (z1 z2 : C), Cdist z1 z2 = R0 <-> z1 = z2.
  Proof.
    intros z1 z2.
    rewrite Cdist_eq_euc_sqr.
    split.
    - intro hyp.
      apply CInj.
      unfold dist_euc in hyp.
      apply sqrt_eq_0 in hyp.
      rewrite Rplus_comm in hyp.
      apply Rplus_sqr_eq_0_l in hyp.
      unfold Rminus in hyp.
      apply (Rplus_eq_compat_r (Im z2)) in hyp.
      field_simplify in hyp.
      exact hyp.
      apply Rplus_le_le_0_compat.
      exact (Rle_0_sqr (Re z1 - Re z2)).
      exact (Rle_0_sqr (Im z1 - Im z2)).
      unfold dist_euc in hyp.
      apply sqrt_eq_0 in hyp.
      apply Rplus_sqr_eq_0_l in hyp.
      apply (Rplus_eq_compat_r (Re z2)) in hyp.
      field_simplify in hyp.
      exact hyp.
      apply Rplus_le_le_0_compat.
      exact (Rle_0_sqr (Re z1 - Re z2)).
      exact (Rle_0_sqr (Im z1 - Im z2)).
    - intro hyp.
      rewrite hyp.
      exact (distance_refl (Re z2) (Im z2)).
  Qed.
  
  Lemma Cdist_symm : forall (z1 z2 : C), Cdist z1 z2 = Cdist z2 z1.
    intros.
    repeat (rewrite Cdist_eq_euc_sqr).
    rewrite distance_symm.
    reflexivity.
  Qed.
  
  Lemma Cdist_pos : forall (z1 z2 : C), ((Cdist z1 z2) >= 0)%R.
  Proof.
    intros.
    rewrite Cdist_eq_euc_sqr.
    unfold dist_euc.
    apply Rle_ge.
    exact (sqrt_pos (_)).
  Qed.

  Lemma Cdist_triang : forall (a b c : C), (Cdist a c <= Cdist a b + Cdist b c)%R.
  Proof.
    intros a b c.
    repeat (rewrite Cdist_eq_euc_sqr).
    Search dist_euc.
    exact (triangle _ _ _ _ _ _).
  Qed.
  
    Lemma Cdist_tri : forall x y z : C, (Cdist x y <= Cdist x z + Cdist z y)%R.
  Proof.
    intros x y z.
    exact (Cdist_triang x z y).
  Qed.
  Definition C_met :=
    Build_Metric_Space C Cdist Cdist_pos Cdist_symm Cdist_refl Cdist_tri.


  Lemma Cdist_good_Re : forall (z z' : C), ((R_dist (Re z) (Re z') <= (Cdist z z'))%R).
  Proof.
    intros.
    unfold Cdist ; unfold R_dist.
    rewrite CNorm2_def.
    unfold dist_euc.
    Search Rabs Rsqr.
    rewrite<- sqrt_Rsqr_abs.
    Search sqrt.
    apply sqrt_le_1_alt.
    repeat (rewrite Rminus_0_l).
    rewrite <- Rplus_0_r at 1.
    apply Rplus_le_compat.
    rewrite<- Rsqr_neg.
    unfold Cminus.
    rewrite Cplus_def_Re.
    rewrite Copp_def_Re.
    unfold Rminus.
    right ; reflexivity.
    Search Rsqr Rle.
    apply Rle_0_sqr.
  Qed.

  Lemma Cdist_good_Im : forall (z z' : C), ((R_dist (Im z) (Im z') <= (Cdist z z'))%R).
  Proof.
    intros.
    unfold Cdist ; unfold R_dist.
    rewrite CNorm2_def.
    unfold dist_euc.
    Search Rabs Rsqr.
    rewrite<- sqrt_Rsqr_abs.
    Search sqrt.
    apply sqrt_le_1_alt.
    repeat (rewrite Rminus_0_l).
    rewrite <- Rplus_0_l at 1.
    apply Rplus_le_compat.
    apply Rle_0_sqr.
    rewrite<- Rsqr_neg.
    unfold Cminus.
    rewrite Cplus_def_Im.
    rewrite Copp_def_Im.
    unfold Rminus.
    right ; reflexivity.
  Qed.

End CDef.


