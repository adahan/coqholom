Load Derivation.

(* RC_integrable signifie "être intégrable" *)

Definition RC_integrable (f : R -> C) (a b : R) :=
  prod (Riemann_integrable (Re_func f) a b)
       (Riemann_integrable (Im_func f) a b).

(* Si p est une preuve que f est RC_integrable entre a et b,
     RC_RiemInt en extrait la valeur de l'intégrale *)

Definition RC_RiemInt {f : R -> C} {a b : R} (p : RC_integrable f a b) :=
  R_to_C (RiemannInt (fst p)) + Ci * R_to_C (RiemannInt (snd p)).


Definition Cfunc_plus {E : Type} (f g : E -> C) := Cplus << f | g.
Definition Cfunc_scal {E : Type} (a : C)(f : E -> C) := (fun z : C => a * z) \u00b0 f.

(***** Propriétés d'extensionnalité *****)
(* D'abord une petite propriété pratique sur les intégrales réelles *)
Proposition RiemannInt_P18' : forall {f g : R -> R} {a b : R} (p : Riemann_integrable f a b)
                                     (q : Riemann_integrable g a b),
    (forall x : R, (Rmin a b < x < Rmax a b)%R -> (f x = g x)) ->
    RiemannInt p = RiemannInt q.
Proof.
  intros.
  case (Rle_dec a b).
  intro.
  apply RiemannInt_P18.
  exact r.
  intros x hypx.
  assert (Rmin a b < x < Rmax a b)%R.
  split.
  Search Rmin.
  rewrite (Rmin_left a b r).
  exact (proj1 hypx).
  rewrite (Rmax_right a b r).
  exact (proj2 hypx).
  exact (H x H0).
  intro.
  apply Rnot_le_lt in n.
  apply Rlt_le in n.
  pose (p' := RiemannInt_P1 p).
  pose (q' := RiemannInt_P1 q).
  rewrite (RiemannInt_P8 p p').
  rewrite (RiemannInt_P8 q q').
  Search Ropp.
  apply Ropp_eq_compat.
  apply RiemannInt_P18.
  exact n.
  intros x hypx.
  assert (Rmin a b < x < Rmax a b)%R.
  split.
  rewrite (Rmin_right a b n).
  exact (proj1 hypx).
  rewrite (Rmax_left a b n).
  exact (proj2 hypx).
  exact (H x H0).
Qed.

(* Maintenant, la même propriété pour les intégrales RC *)

Proposition RCRI_P18' {f g : R -> C} {a b : R} (p : RC_integrable f a b) 
            (q : RC_integrable g a b) :
  (forall x : R, (Rmin a b < x < Rmax a b)%R -> f x = g x) ->
  RC_RiemInt p = RC_RiemInt q.
Proof.
  intros.
  unfold RC_RiemInt.
  destruct p ; destruct q ; simpl.
  apply CInj.
  repeat (rewrite Cplus_def_Im).
  repeat (rewrite Cmult_def_Im).
  repeat (rewrite RtoC_noIm).
  repeat (rewrite RInj).
  repeat (rewrite Ci_def_Im).
  repeat (rewrite Ci_def_Re).
  field_simplify.
  apply RiemannInt_P18'.
  intros x hypx.
  unfold Im_func ; unfold comp.
  rewrite (H x hypx).
  reflexivity.
  repeat (rewrite Cplus_def_Re).
  repeat (rewrite Cmult_def_Re).
  repeat (rewrite RInj).
  repeat (rewrite RtoC_noIm).
  repeat (rewrite Ci_def_Im).
  repeat (rewrite Ci_def_Re).
  field_simplify.
  apply RiemannInt_P18'.
  intros x hypx.
  unfold Re_func ; unfold comp.
  rewrite (H x hypx).
  reflexivity.
Qed.

(***** Linéarité de l'intégrale *****)
Lemma Rminusaux : forall x y : R, (x + - y = x - y)%R.
Proof.
  unfold Rminus ; reflexivity.
Qed.
Proposition RC_integrable_scal : forall {f : R -> C} {a b : R} (u : C),
    RC_integrable f a b -> RC_integrable (Cfunc_scal u f) a b.
Proof.
  intros.
  unfold RC_integrable.
  unfold Re_func.
  unfold Im_func.
  unfold Cfunc_scal.
  unfold comp.
  split.
  pose (Ref := (fun a0 : R => (Re u) * (Re (f a0)) + (1 * (-(Im u) * (Im (f a0)))))%R).
  assert (Riemann_integrable Ref  a b).
  apply (RiemannInt_P10 1%R).
  exact (Riemann_integrable_scal (Re u) (fst X)).
  exact (Riemann_integrable_scal (- Im u) (snd X)).
  assert (forall a0 : R, Ref a0 = Re (u * f a0)).
  intro x.
  unfold Ref.
  rewrite Rmult_1_l.
  Search Rmult Ropp.
  rewrite Ropp_mult_distr_l_reverse.
  rewrite (Rminusaux (Re u * Re (f x)) (Im u * Im (f x))).
  rewrite Cmult_def_Re.
  field.
  Print Riemann_integrable_ext.
  apply (Riemann_integrable_ext (fun a0 : R => Re (u * f a0)) (fun x hyp => H x) X0).
  pose (Imf := (fun a0 : R => (Im u) * (Re (f a0)) + 1 * ((Re u) * (Im (f a0))))%R).
  assert (Riemann_integrable Imf a b).
  apply (RiemannInt_P10 1%R).
  exact (Riemann_integrable_scal (Im u) (fst X)).
  exact (Riemann_integrable_scal (Re u) (snd X)).
  assert (forall a0 : R, Imf a0 = Im (u * f a0)).
  intro x.
  unfold Imf.
  rewrite Rmult_1_l.
  rewrite (Rmult_comm (Re u) _) .
  rewrite Cmult_def_Im.
  field.
  apply (Riemann_integrable_ext (fun a0 : R => Im (u * f a0)) (fun x hyp => H x) X0).
  (* Ce dernier théorème utilisé permet de ne pas se servir de l'axiome d'extensionnalité *)
Qed.

Proposition RC_integrable_plus : forall {f g : R -> C} {a b : R},
    RC_integrable f a b ->  RC_integrable g a b -> 
    RC_integrable (Cfunc_plus f g) a b.
Proof.
  intros.
  unfold Cfunc_plus.
  unfold comp2.
  unfold RC_integrable.
  unfold Re_func.
  unfold Im_func.
  unfold comp.
  pose (Res := (fun a0 : R => (Re (f a0)) + 1 * (Re (g a0)%C))%R).
  assert (Riemann_integrable Res a b).
  apply (RiemannInt_P10 1%R).
  exact (fst X).
  exact (fst X0).
  assert (forall a0 : R, Res a0 = Re (f a0 + g a0)).
  intro x ; unfold Res ; rewrite Cplus_def_Re ; field.
  pose (Ims := (fun a0 : R => (Im (f a0)) + 1 * (Im (g a0)))%R).
  assert (Riemann_integrable Ims a b).
  apply (RiemannInt_P10 1%R).
  exact (snd X).
  exact (snd X0).
  assert (forall a0 : R, Ims a0 = Im (f a0 + g a0)).
  intro x ; unfold Ims ; rewrite Cplus_def_Im ; field.
  split.
  apply (Riemann_integrable_ext _ (fun x hyp => H x) X1).
  apply (Riemann_integrable_ext _ (fun x hyp => H0 x) X2).
Qed.

Proposition RC_integrable_linear : forall {f g : R -> C} {a b : R} (u v : C),
    RC_integrable f a b -> RC_integrable g a b ->
    RC_integrable (Cfunc_plus (Cfunc_scal u f) (Cfunc_scal v g)) a b.
Proof.
  intros.
  apply RC_integrable_plus ; apply RC_integrable_scal.
  exact X.
  exact X0.
Qed.

(* On a montré que l'intégrabilité était stable par combinaisons linéaires.
     Montrons que l'intégrale  \u00e0 proprement parler est linéaire : *)

Proposition tap_Riemann_integrable_ext {a b : R} {f : R -> R} (g : R -> R) (pr : Riemann_integrable f a b) : (forall (x : R) (p : (Rmin a b <= x <= Rmax a b)%R), f x = g x) -> (Riemann_integrable g a b).
Proof.
  intros.
  apply (Riemann_integrable_ext g H pr). 
Qed.
Proposition RC_integrable_0 : forall a b : R, RC_integrable (fun x => C0) a b.
Proof.
  intros.
  unfold RC_integrable.
  unfold Re_func ; unfold Im_func ; unfold comp.
  assert (Riemann_integrable (fct_cte 0) a b).
  exact (RiemannInt_P14 a b 0).
  split.
  apply (tap_Riemann_integrable_ext _ X).
  intros x hyp.
  rewrite C0_def.
  rewrite RInj.
  reflexivity.
  apply (tap_Riemann_integrable_ext _ X).
  rewrite C0_def.
  rewrite RtoC_noIm.
  reflexivity.
Qed.


Proposition RCRI_scal : forall {f : R -> C} {a b : R} (u : C) (p : RC_integrable f a b)
                               (pu : RC_integrable (Cfunc_scal u f) a b),
    (RC_RiemInt pu = u * RC_RiemInt p).
Proof.
  intros.
  unfold RC_RiemInt.
  apply CInj.
  * repeat (rewrite Cplus_def_Im).
    repeat (rewrite Cmult_def_Im).
    repeat (rewrite Cplus_def_Re).
    repeat (rewrite Cmult_def_Re).
    repeat (rewrite RtoC_noIm).
    repeat (rewrite Ci_def_Re).
    repeat (rewrite Ci_def_Im).
    repeat (rewrite Cplus_def_Im).
    repeat (rewrite RInj).
    repeat (rewrite RtoC_noIm).
    rewrite Cmult_def_Im.
    rewrite RInj.
    rewrite RtoC_noIm.
    rewrite Ci_def_Re ; rewrite Ci_def_Im.
    field_simplify.
    destruct p ; destruct pu.
    simpl.
    rewrite <- Rplus_0_l.
    rewrite <- (Rmult_0_l (b - a)).
    pose (nulle := RiemannInt_P14 a b 0).
    rewrite <- (RiemannInt_P15 nulle).
    rewrite <-  Rplus_assoc.
    rewrite (Rmult_comm _ (Re u)).
    pose (middlesum := RiemannInt_P10 (Im u) nulle r).
    rewrite <- (RiemannInt_P13 (nulle) r middlesum).
    pose (lastsum := RiemannInt_P10 (Re u) middlesum r0).
    rewrite <- (RiemannInt_P13 middlesum r0 lastsum).
    simpl in lastsum.
    apply RiemannInt_P18'.
    intros x hypx.
    unfold Im_func ; unfold Cfunc_scal ; unfold fct_cte ; unfold Re_func.
    unfold comp.
    rewrite Cmult_def_Im.
    field.
  * repeat (rewrite Cplus_def_Re).
    repeat (rewrite Cmult_def_Re).
    repeat (rewrite Cplus_def_Im).
    repeat (rewrite Cmult_def_Im).
    repeat (rewrite Cplus_def_Re).
    repeat (rewrite Cmult_def_Re).
    repeat (rewrite RInj).
    repeat (rewrite RtoC_noIm).
    repeat (rewrite Ci_def_Re).
    repeat (rewrite Ci_def_Im).
    field_simplify.
    destruct p ; destruct pu.
    simpl.
    unfold Rminus.
    rewrite <- Rplus_0_l.
    rewrite <- (Rmult_0_l (b - a)).
    pose (nulle := RiemannInt_P14 a b 0).
    rewrite <- (RiemannInt_P15 nulle).
    rewrite <- Rplus_assoc.
    rewrite (Rmult_comm _ (Im u)).
    pose (middlesum := RiemannInt_P10 (Re u) nulle r).
    rewrite <- (RiemannInt_P13 nulle r middlesum).
    Search Ropp Rmult.
    rewrite Ropp_mult_distr_l.
    pose (lastsum := RiemannInt_P10 (-Im u) middlesum r0).
    rewrite <- (RiemannInt_P13 middlesum r0 lastsum).
    apply RiemannInt_P18'.
    intros x hypx.
    unfold Im_func ; unfold Cfunc_scal ; unfold fct_cte ; unfold Re_func.
    unfold comp.
    rewrite Cmult_def_Re.
    field.
Qed.

Proposition RCRI_plus : forall {f g : R -> C} {a b : R} (p : RC_integrable f a b)
                               (q : RC_integrable g a b) (r : RC_integrable (Cfunc_plus f g) a b),
    RC_RiemInt r = RC_RiemInt p + RC_RiemInt q.
Proof.
  intros.
  unfold RC_RiemInt.
  apply CInj.
  repeat (rewrite Cplus_def_Im).
  repeat (rewrite RtoC_noIm).
  repeat (rewrite Cmult_def_Im).
  repeat (rewrite Ci_def_Im).
  repeat (rewrite RtoC_noIm).
  repeat (rewrite RInj).
  repeat (rewrite Ci_def_Re).
  field_simplify.
  destruct p ; destruct q ; destruct r ; simpl.
  rewrite <- (Rmult_1_l (RiemannInt r3)). 
  pose (sum := RiemannInt_P10 1 r1 r3).
  rewrite <- (RiemannInt_P13 r1 r3 sum).
  apply RiemannInt_P18'.
  intros x hypx.
  unfold Im_func ; unfold Cfunc_plus.
  unfold comp2 ; unfold comp.
  rewrite Cplus_def_Im.
  field.
  repeat (rewrite Cplus_def_Re).
  repeat (rewrite Cmult_def_Re).
  repeat (rewrite RInj).
  repeat (rewrite Ci_def_Re).
  repeat (rewrite RtoC_noIm).
  field_simplify.
  destruct p ; destruct q ; destruct r ; simpl.
  rewrite <- (Rmult_1_l (RiemannInt r2)).
  pose (sum := RiemannInt_P10 1 r0 r2).
  rewrite <- (RiemannInt_P13 r0 r2 sum).
  apply RiemannInt_P18'.
  intros x hypx.
  unfold Re_func ; unfold Cfunc_plus.
  unfold comp ; unfold comp2.
  rewrite Cplus_def_Re.
  field.
Qed.
Proposition RCRI_linear : forall {f g : R -> C} {a b : R} {u v : C}  (p : RC_integrable f a b)
                                 (q : RC_integrable g a b)
                                 (r : RC_integrable (Cfunc_plus (Cfunc_scal u f) (Cfunc_scal v g))
                                                    a b),
    RC_RiemInt r = u * (RC_RiemInt p) + v * (RC_RiemInt q).
Proof.
  intros.
  pose (pu := RC_integrable_scal u p).
  rewrite <- (RCRI_scal u p pu).
  pose (qv := RC_integrable_scal v q).
  rewrite <- (RCRI_scal v q qv).
  rewrite <- (RCRI_plus pu qv (RC_integrable_plus pu qv)).
  apply RCRI_P18'.
  intros x hypx.
  unfold Cfunc_plus ; unfold Cfunc_scal.
  unfold comp2 ; unfold comp.
  field.
Qed.

(***** Relation de Chasles et associé *****)

Proposition RCRI_P1 {f : R -> C} {a b : R} : forall p : RC_integrable f a b,
    RC_integrable f b a.
Proof.
  intros.
  unfold RC_integrable.
  split.
  exact (RiemannInt_P1 (fst p)).
  exact (RiemannInt_P1 (snd p)).
Qed.
Print RiemannInt_P23.
Proposition RCRI_P23 {f : R -> C} {a b c : R} :
  RC_integrable f a b -> (a <= c <= b)%R -> RC_integrable f c b.
Proof.
  intros.
  unfold RC_integrable.
  split.
  exact (RiemannInt_P23 (fst X) H).
  exact (RiemannInt_P23 (snd X) H).
Qed.

(***** Théorème fondamental de l'analyse *****)

(* On avait besoin de ces résultats de continuité pour ce résultat important : *)

Theorem RCFTC_P1 : forall {f : R -> C} {a b : R} (p : (a <= b)%R),
    (forall x : R, (a <= x <= b)%R -> RC_continuity_pt f x) ->
    forall x : R, (a<= x)%R -> (x <= b)%R -> RC_integrable f a x.
Proof.
  intros.
  unfold RC_integrable.
  split.
  apply (FTC_P1 p).
  intros x0 hypx0.
  apply continuity_is_good_Re.
  apply H.
  exact hypx0.
  exact H0.
  exact H1.
  apply (FTC_P1 p).
  intros x0 hypx0.
  apply continuity_is_good_Im.
  apply H.
  exact hypx0.
  exact H0.
  exact H1.
Qed.
Theorem RCFTC_P1' : forall (f : R -> C) (a b : R),
    (forall x : R, (Rmin a b <= x <= Rmax a b)%R -> RC_continuity_pt f x) ->
    forall x : R, (Rmin a b <= x)%R -> (x <= Rmax a b)%R -> RC_integrable f a x.
Proof.
  intros.
  case (Rle_dec a b).
  * intro p.
    apply (RCFTC_P1 p).
    intros x0 hypx0.
    destruct hypx0.
    apply H.
    exact (conj (Rle_trans _ _ _ (Rmin_l a b) H2) (Rle_trans _ _ _ H3 (Rmax_r a b))).
    rewrite <- (Rmin_left a b p).
    exact H0.
    rewrite <- (Rmax_right a b p).
    exact H1.
  * intro p.
    apply Rnot_le_lt in p.
    apply Rlt_le in p.
    assert (RC_integrable f b a).
    apply (RCFTC_P1 p).
    intros.
    apply H.
    exact (conj (Rle_trans _ b x0 (Rmin_r a b) (proj1 H2))
                (Rle_trans x0 a _ (proj2 H2) (Rmax_l a b))).
    exact p.
    exact (Rle_refl a).
    apply RCRI_P1.
    apply (RCRI_P23 X).
    rewrite<- (Rmin_right a b p).
    rewrite<- (Rmax_left a b p) at 2.
    exact (conj H0 H1).
Qed.


Theorem derive_is_good_Re : forall (f : R -> C) (pr : RC_derivable f)
                                   (prRe : derivable (Re_func f)) (x : R),
    derive (Re_func f) prRe x = Re (RC_derive f pr x).

Proof.
  intros.
  unfold derive ; unfold RC_derive.
  (* unfold derive_pt ; rewrite*)
  destruct (prRe x) as [x0 limitx0].
  destruct (pr x) as [z0 limitz0].
  simpl in *.
  unfold derivable_pt_abs in limitx0.
  unfold derivable_pt_lim in limitx0.
  unfold limit_in in limitz0.
  apply cond_eq.
  intros eps hypeps.
  assert (0 < eps / 2)%R.
  Search Rmult Rlt.
  apply Rmult_lt_0_compat.
  exact hypeps.
  Search Rinv Rlt.
  apply Rinv_0_lt_compat.
  prove_sup.
  pose (goodx0 := limitx0 (eps / 2)%R H).
  pose (goodz0 := limitz0 (eps / 2)%R H).
  destruct goodx0.
  destruct goodz0.
Admitted.
Print RCC1.

Theorem RCFTC : forall (f : R -> C) (a b : R) (p : RCC1 f)
                       (pr : RC_integrable (RC_derive f (proj1_sig p)) a b),
    RC_RiemInt pr = f b - f a.
Proof.
  intros.
  apply CInj.
  unfold RC_RiemInt.
  unfold Cminus.
  repeat (rewrite Cplus_def_Im).
  repeat (rewrite RtoC_noIm).
  rewrite Copp_def_Im.
  repeat (rewrite Cmult_def_Im).
  repeat (rewrite RInj ; rewrite RtoC_noIm).
  rewrite Ci_def_Im.
  field_simplify.
  destruct pr ; simpl in *.
  apply FTC_Riemann.

  (* 
    Proposition Rdiff_good : forall (U : R -> Prop ) (f : R -> R) (x : R),
        derivable_pt f x -> {l : R | limit1_in (R_differentiate f x) (D_x R no_cond x) l x }.
    Proof.
      intros.
      destruct H as [l limit].
      exists l.
      unfold limit1_in.
      unfold limit_in ; unfold R_met ; unfold dist.
      intros eps hypeps.
      pose (etaproof := limit eps hypeps).
      destruct etaproof as [eta goodeta].
      exists eta.
      simpl in * .
      split.
      Print posreal.
      exact (Rlt_gt _ _ (cond_pos eta)).
      intros x1 [D rdistx1].
      unfold R_differentiate.
      pose (h := (x1 - x)%R).
      fold h.
      unfold R_dist.
      Search Rminus Rplus.
      rewrite <- (Rplus_minus x x1).
      fold h.
      apply goodeta.
      unfold D_x in D.
      unfold h.
      Search Rminus eq not.
      apply Rminus_eq_contra.
      exact (not_eq_sym (proj2 D)).
      unfold R_dist in rdistx1.
      fold h in rdistx1.
      exact rdistx1.
    Qed.

    Proposition Rdiff_good_alt : forall (f : R -> R) (x : R),
        {l : R | limit1_in (R_differentiate f x) (D_x R no_cond x) l x} -> derivable_pt f x.
    Proof.
      intros.
      unfold derivable_pt.
      destruct H as [l limit].
      exists l.
      unfold derivable_pt_abs ; unfold derivable_pt_lim.
      intros eps hypeps.
      pose (etapeta := limit eps hypeps).
      destruct etapeta as [eta [etagt0 etagood]].
      exists (mkposreal eta etagt0).
      intros h hyph disth.
      unfold dist in etagood ; unfold R_met in etagood ; unfold R_differentiate in etagood.
      simpl in etagood.
      unfold R_dist in etagood.
      pose (x0 := (x + h)%R).
      fold x0.
      assert (h = x0 - x)%R.
      unfold x0 ; field.
      rewrite H.
      apply etagood.
      unfold D_x.
      split.
      split.
      exact I.
      unfold x0.
      intro.
      apply hyph.
      Print Rplus_eq_reg_l.
      rewrite <- Rplus_0_r in H0 at 1.
      apply Rplus_eq_reg_l in H0.
      rewrite H0 ; reflexivity.
      rewrite<- H.
      exact disth.
    Qed. *)
