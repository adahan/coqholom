Load Analyse.

Definition subset {T : Type} (E F : T -> Prop) := forall x, E x -> F x.

Proposition subset_continuity : forall (f : R -> C) (E F : R -> Prop),
    (continue_in R_met C_met f F) -> (subset E F) -> (continue_in R_met C_met f E).
Proof.
  unfold subset.
  intros.
  unfold continue_in.
  intros.
  unfold continue_at.
  unfold limit_in.
  intros.
  pose (etaproof := H x0 (H0 x0 H1) eps H2).
  destruct etaproof as [eta [etagt0 etagood]].
  exists eta.
  split.
  exact etagt0.
  intros x [Dx distxx0].
  apply (etagood x).
  split.
  split.
  exact (H0 x (proj1 Dx)).
  exact (proj2 Dx).
  exact distxx0.
Qed.

(* Considérations plus faciles sur les dérivées partielles d'une fonction complexes *)
Definition Re_func (f : R -> C) := Re ° f.
Definition Im_func (f : R -> C) := Im ° f.
Proposition Re_cont : continue C_met R_met Re.
Proof.
  unfold continue.
  unfold continue_at.
  unfold limit_in.
  intros x0 eps hypeps.
  exists eps.
  split.
  exact hypeps.
  intros x [_ distx].
  unfold dist in distx.
  unfold C_met in distx.
  unfold Cdist in distx.
  rewrite CNorm2_def in distx.
  unfold dist ; unfold R_met ; unfold R_dist.
  unfold dist_euc in distx.
  Search Rlt Rplus 0%R.
  apply (Rle_lt_trans _ (sqrt (Rsqr (0 - Re (x - x0)%C)+ Rsqr (0 - Im (x - x0)%C))) eps).
  rewrite <- sqrt_Rsqr_abs.
  Search sqrt Rle.
  apply sqrt_le_1_alt.
  Search Rplus Rle.
  rewrite <- Rplus_0_r at 1.
  apply (Rplus_le_compat _  _ 0 (Rsqr (0 - Im (x - x0)%C))).
  unfold Cminus.
  Search Re.
  rewrite Cplus_def_Re.
  rewrite Copp_def_Re.
  Search Rsqr.
  unfold Rminus.
  rewrite Rplus_0_l.
  rewrite Rsqr_neg.
  exact (Rle_refl _).
  Search Rsqr.
  exact (Rle_0_sqr _).
  exact distx.
Qed.
Proposition Im_cont : continue C_met R_met Im.
Proof.
  unfold continue.
  unfold continue_at.
  unfold limit_in.
  intros x0 eps hypeps.
  exists eps.
  split.
  exact hypeps.
  intros x [_ distx].
  unfold dist in distx.
  unfold C_met in distx.
  unfold Cdist in distx.
  rewrite CNorm2_def in distx.
  unfold dist ; unfold R_met ; unfold R_dist.
  unfold dist_euc in distx.
  Search Rlt Rplus 0%R.
  apply (Rle_lt_trans _ (sqrt (Rsqr (0 - Re (x - x0)%C)+ Rsqr (0 - Im (x - x0)%C))) eps).
  rewrite <- sqrt_Rsqr_abs.
  rewrite Rplus_comm.
  Search sqrt Rle.
  apply sqrt_le_1_alt.
  Search Rplus Rle.
  rewrite <- Rplus_0_r at 1.
  apply (Rplus_le_compat _  _ 0 (Rsqr (0 - Re (x - x0)%C))).
  unfold Cminus.
  Search Re.
  rewrite Cplus_def_Im.
  rewrite Copp_def_Im.
  Search Rsqr.
  unfold Rminus.
  rewrite Rplus_0_l.
  rewrite Rsqr_neg.
  exact (Rle_refl _).
  Search Rsqr.
  exact (Rle_0_sqr _).
  exact distx.
Qed.

Search "continue".
Proposition continuity_weaken {E F : Metric_Space} :
  forall (f : Base E -> Base F) (U : Base E -> Prop) (V : Base E -> Prop),
    continue_in E F f U -> (forall x : Base E, V x -> U x) -> continue_in E F f V.
Proof.
  intros.
  unfold continue_in.
  unfold continue_at.
  unfold limit_in.
  intros.
  pose (etaproof := H x0 (H0 x0 H1) eps H2).
  destruct etaproof as [eta [etagt0 etagood]].
  exists eta.
  split.
  exact etagt0.
  intros x [Dxx0 distx].
  apply etagood.
  split.
  unfold D_x.
  split.
  exact (H0 x (proj1 Dxx0)).
  exact (proj2 Dxx0).
  exact distx.
Qed.

Axiom dec_equality_for_metric_spaces : forall (M : Metric_Space), forall (m m' : Base M),
      {m = m'} + {m <> m'}.
(* On doit pouvoir se passer de cet axiome très fort, mais il est, en quelque sorte, utilisé
   dans la bibliothèque Reals *)

Proposition continuity_comp (E F G : Metric_Space) :
  forall (f : Base E -> Base F) (g : Base F -> Base G) (U : Base E -> Prop) (V : Base F -> Prop),
    continue_in E F f U -> continue_in F G g V -> (forall x : Base E, U x -> V (f x)) ->
    continue_in E G (g ° f) U.
Proof.
  intros.
  unfold continue_in.
  intros x0 hypx0.
  unfold continue_at.
  unfold limit_in.
  intros eps hypeps.
  pose (metaproof := H0 (f x0) (H1 x0 hypx0) eps hypeps).
  destruct metaproof as [meta [metagt0 metagood]].
  pose (etaproof := H x0 hypx0 meta metagt0).
  destruct etaproof as [eta [etagt0 etagood]].
  exists eta.
  split.
  exact etagt0.
  intros x [Dxx distx].
  unfold D_x in Dxx.
  case (dec_equality_for_metric_spaces F (f x) (f x0)).
  * intro.
    unfold comp ; rewrite e.
    Search Metric_Space.
    rewrite (proj2 (dist_refl G _ _) (eq_refl _ )).
    exact hypeps.
  * intro.
    apply metagood.
    split.
    unfold D_x.
    split.
    exact (H1 x (proj1 Dxx)).
    intro.
    rewrite H2 in n.
    apply n ; reflexivity.
    apply etagood.
    split.
    exact Dxx.
    exact distx.
Qed.
Definition continue_en (f : R -> C) (U : R -> Prop) := continue_in R_met C_met f U.
Proposition Re_func_cont : forall (f : R -> C) (U : R -> Prop), 
    (continue_in R_met C_met f U -> continue_in R_met R_met (Re_func f) U).
Proof.
  intros.
  unfold Re_func.
  apply (continuity_comp R_met C_met R_met f Re U (fun x => True)).
  exact H.
  pose (hyp := Re_cont).
  unfold continue_in.
  intro x.
  intro.
  exact (hyp x).
  exact (fun x p => I).
Qed.
Proposition Im_func_cont : forall (f : R -> C) (U : R -> Prop),
    (continue_in R_met C_met f U -> continue_in R_met R_met (Im_func f) U).
Proof.
  intros.
  unfold Im_func.
  apply (continuity_comp R_met C_met R_met f Im U (fun x => True)).
  exact H.
  pose (hyp := Im_cont).
  unfold continue in hyp.
  unfold continue_in.
  intro x.
  intro.
  exact (hyp x).
  exact (fun x p => I).
Qed.

Definition RC_continuity_pt (f : R -> C) (x0 : R) :=
  continue_at R_met C_met f no_cond x0.

Proposition continuity_is_good_Re : forall (f : R -> C) (x0 : R),
    RC_continuity_pt f x0 -> continuity_pt (Re_func f) x0.
Proof.
  intros.
  unfold RC_continuity_pt.
  unfold continuity_pt.
  unfold Rderiv.continue_in.
  unfold limit1_in.
  unfold Re_func ; unfold comp.
  unfold limit_in.
  intros eps hypeps.
  unfold continue_at in H.
  unfold limit_in in H.
  pose (etaproof := H eps hypeps).
  destruct etaproof as [eta [hypeta etagood]].
  exists eta.
  split.
  exact hypeta.
  intros x hypsx.
  pose (etainC := etagood x hypsx).
  apply (Rle_lt_trans _ (dist C_met (f x) (f x0)) eps).
  unfold dist ; unfold R_met ; unfold C_met ; simpl.
  unfold Cdist.
  rewrite CNorm2_def.
  unfold dist_euc.
  apply (Rle_trans _ (sqrt (Rsqr (0 - Re (f x - f x0)%C))) _).
  unfold R_dist.
  unfold Rminus.
  rewrite Rplus_0_l.
  rewrite <- Rsqr_neg.
  Search Rsqr sqrt.
  rewrite sqrt_Rsqr_abs.
  unfold Cminus.
  rewrite Cplus_def_Re.
  rewrite Copp_def_Re.
  exact (Rle_refl _).
  Search sqrt Rle.
  apply sqrt_le_1.
  Search Rsqr.
  apply Rle_0_sqr.
  Search Rle Rplus 0%R.
  apply Rplus_le_le_0_compat.
  apply Rle_0_sqr.
  apply Rle_0_sqr.
  rewrite <- Rplus_0_r at 1.
  apply Rplus_le_compat.
  apply Rle_refl.
  apply Rle_0_sqr.
  apply etagood.
  exact hypsx.
Qed.
Proposition continuity_is_good_Im : forall (f : R -> C) (x0 : R),
    RC_continuity_pt f x0 -> continuity_pt (Im_func f) x0.
Proof.
  intros.
  unfold RC_continuity_pt.
  unfold continuity_pt.
  unfold Rderiv.continue_in.
  unfold limit1_in.
  unfold Im_func ; unfold comp.
  unfold limit_in.
  intros eps hypeps.
  unfold continue_at in H.
  unfold limit_in in H.
  pose (etaproof := H eps hypeps).
  destruct etaproof as [eta [hypeta etagood]].
  exists eta.
  split.
  exact hypeta.
  intros x hypsx.
  pose (etainC := etagood x hypsx).
  apply (Rle_lt_trans _ (dist C_met (f x) (f x0)) eps).
  unfold dist ; unfold R_met ; unfold C_met ; simpl.
  unfold Cdist.
  rewrite CNorm2_def.
  unfold dist_euc.
  apply (Rle_trans _ (sqrt (Rsqr (0 - Im (f x - f x0)%C))) _).
  unfold R_dist.
  unfold Rminus.
  rewrite Rplus_0_l.
  rewrite <- Rsqr_neg.
  Search Rsqr sqrt.
  rewrite sqrt_Rsqr_abs.
  unfold Cminus.
  rewrite Cplus_def_Im.
  rewrite Copp_def_Im.
  exact (Rle_refl _).
  Search sqrt Rle.
  apply sqrt_le_1.
  Search Rsqr.
  apply Rle_0_sqr.
  Search Rle Rplus 0%R.
  apply Rplus_le_le_0_compat.
  apply Rle_0_sqr.
  apply Rle_0_sqr.
  rewrite <- Rplus_0_l at 1.
  apply Rplus_le_compat.
  apply Rle_0_sqr.
  apply Rle_refl.
  apply etagood.
  exact hypsx.
Qed.

