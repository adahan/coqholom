Load Derivation.
Require Import Rlimit.
Require Import DiscrR.
Require Lia.
Open Scope C_scope.


Module Chemins.
  Definition M := C_met.
  Definition interval (a b : R) := (fun x : R => (a <= x <= b))%R.
  Proposition interval_deb : forall (a b : R), (a <= b)%R -> (interval a b a).
  Proof.
    intros.
    exact (conj (Rle_refl a) H).
  Qed.
  Proposition interval_fin : forall (a b : R), (a <= b)%R -> (interval a b b).
  Proof.
    intros.
    exact (conj H (Rle_refl b)).
  Qed.
  Proposition interval_mil : forall (a b m1 m2 : R),
      (a <= m1)%R -> (m2 <= b)%R ->
      subset (interval m1 m2) (interval a b).
  Proof.
    unfold subset.
    intros.
    exact (conj (Rle_trans a m1 x H (proj1 H1)) (Rle_trans x m2 b (proj2 H1) H0)).
  Qed.

  Definition chemin (a b : R) := {gamma : R -> Base M | continue_in R_met M gamma (interval a b)}.
  Definition debut {a b : R} (gamma : chemin a b) := (proj1_sig gamma) a%R.
  Definition fin {a b : R} (gamma : chemin a b) := (proj1_sig gamma) b%R.
  Definition compat {a b a' b' : R} 
             (gamma1 : chemin a b) (gamma2 : chemin a' b') := (fin gamma1) = (debut gamma2).

  Definition concat {a b a' b' : R} (gamma1 : chemin a b) (gamma2 : chemin a' b') :=
    (fun (t : R) => if Rle_dec t b 
                    then (proj1_sig gamma1) t
                    else (proj1_sig gamma2) (t -b + a')%R).

  Lemma concat_is_continuous : forall (a b a' b' : R) (g1 : chemin a b) (g2 : chemin a' b'),
      (compat g1 g2) -> continue_in R_met M  (concat g1 g2) (interval a (b + b' - a')%R).
  Proof.
    intros a b a' b'.
    intros [gamma1 cont1] [gamma2 cont2].
    unfold compat.
    unfold debut ; unfold fin.
    simpl.
    intro compatHyp.
    unfold concat ; simpl.
    unfold continue_in.
    intro x_0 ; intro hypx0.
    unfold continue_at ; unfold limit_in.
    intros eps hypeps.
    case (Rlt_dec x_0 b).
    * intro x_0ltb.
      assert (interval a b x_0).
      split.
      exact (proj1 hypx0).
      exact (Rlt_le _ _ x_0ltb).
      pose (eta'proof := cont1 x_0 H eps hypeps).
      destruct eta'proof as [eta' [eta'gt0 eta'good]].
      pose (eta := Rmin eta' (b - x_0)%R).
      exists eta.
      split.
      unfold eta.
      apply Rmin_Rgt_r.
      split.
      exact eta'gt0.
      apply Rgt_minus.
      unfold Rgt.
      exact (x_0ltb).
      unfold D_x.
      intros x [[interval_x xnex0] dist_x_x0_eta].
      unfold dist in dist_x_x0_eta ; unfold R_met in dist_x_x0_eta.
      induction (Rle_dec x b).
      induction (Rle_dec x_0 b).
      apply eta'good.
      unfold D_x.
      split.
      split.
      split.
      exact (proj1 interval_x).
      exact a0.
      exact xnex0.
      Search Rle Rmin.
      exact (Rlt_le_trans _ eta eta' dist_x_x0_eta (Rmin_l eta' _)).
      exfalso.
      apply b0.
      exact (proj2 H).
      Search Rlt Rle.
      exfalso.
      apply b0.
      unfold R_dist in dist_x_x0_eta.
      apply (Rlt_le_trans (Rabs (x - x_0))  eta (b - x_0))%R in dist_x_x0_eta.
      Search Rabs.
      apply (Rle_lt_trans (x - x_0) (Rabs (x - x_0)) (b - x_0))%R in dist_x_x0_eta.
      unfold Rminus in dist_x_x0_eta.
      Search Rplus Rlt.
      apply (Rplus_lt_compat_r x_0 _ _) in dist_x_x0_eta.
      field_simplify in dist_x_x0_eta.
      exact (Rlt_le _ _ dist_x_x0_eta).
      Search Rabs.
      exact (Rle_abs _).
      exact (Rmin_r _ _).
    * intro hyp.
      apply Rnot_lt_le in hyp.
      Search Rle Rlt.
      case (Rle_lt_or_eq_dec b x_0 hyp).
      intro bltx0.
      assert (interval a' b' (x_0 - b + a')%R).
      split.
      rewrite<- Rplus_0_l at 1.
      apply (Rplus_le_compat_r a').
      apply Rge_le.
      apply Rge_minus.
      apply Rle_ge.
      exact hyp.
      apply (Rplus_le_reg_r (b - a') (x_0 - b + a') b').
      field_simplify.
      unfold Rminus.
      rewrite Rplus_assoc.
      rewrite (Rplus_comm (-a') (b'))%R.
      rewrite <- Rplus_assoc.
      exact (proj2 hypx0).
      pose (eta'proof := cont2 (x_0 - b + a')%R H eps hypeps).
      destruct eta'proof as [eta' [eta'gt0 goodeta']].
      pose (eta := Rmin eta' (x_0 - b)).
      exists eta.
      split.
      apply Rmin_Rgt.
      split.
      exact eta'gt0.
      apply Rgt_minus.
      unfold Rgt.
      exact bltx0.
      unfold D_x.
      intros x [[interval_x x0_ne_x] dist_x_x0].
      unfold dist in dist_x_x0 ; unfold R_met in dist_x_x0.
      induction (Rle_dec x b).
      induction (Rle_dec x_0 b).
      exfalso.
      apply (Rlt_not_le _ _ bltx0).
      exact a1.
      exfalso.
      apply (Rle_not_lt _ _ a0).
      unfold R_dist in dist_x_x0.
      apply (Rlt_le_trans (Rabs (x - x_0)) eta (x_0 - b))%R in dist_x_x0.
      apply (Rle_lt_trans (x_0 - x) (Rabs (x - x_0)) (x_0 - b))%R in dist_x_x0.
      Search Rminus Rlt.
      apply Rlt_minus in dist_x_x0.
      Search Rplus Rlt.
      apply (Rplus_lt_compat_l x)%R in dist_x_x0.
      field_simplify in dist_x_x0.
      exact dist_x_x0.
      Search Rabs Rminus.
      rewrite Rabs_minus_sym.
      exact (Rle_abs _).
      exact (Rmin_r _ _).
      induction (Rle_dec x_0 b).
      exfalso.
      apply (Rle_not_lt _ _ a0).
      exact bltx0.
      apply (goodeta' (x - b + a')%R).
      split.
      unfold D_x.
      split.
      split.
      Search Rle Rminus.
      apply Rminus_le.
      field_simplify.
      apply (Rplus_le_reg_l x).
      field_simplify.
      exact (Rlt_le _ _ (Rnot_le_lt _ _ b0)).
      apply (Rplus_le_reg_r (b - a')).
      field_simplify.
      unfold Rminus.
      rewrite Rplus_assoc.
      rewrite (Rplus_comm (-a')%R b'). 
      rewrite <- Rplus_assoc.
      exact (proj2 interval_x).
      intro.
      apply (Rplus_eq_compat_r (b - a')%R) in H0.
      field_simplify in H0.
      exact (x0_ne_x H0).
      unfold dist.
      unfold R_met.
      unfold R_dist.
      assert ((x - b + a' - (x_0 - b + a')) = x - x_0)%R.
      field_simplify.
      reflexivity.
      rewrite H0.
      unfold R_dist in dist_x_x0.
      apply (Rlt_le_trans (Rabs (x - x_0)) eta eta').
      exact dist_x_x0.
      exact (Rmin_l _ _).
      intro stronghyp.
      assert (eps/2 > 0)%R as hypeps2.
      unfold Rgt.
      Search Rdiv Rlt.
      apply (Rdiv_lt_0_compat eps 2).
      unfold Rgt in hypeps.
      exact hypeps.
      prove_sup.
      pose (eta1proof := cont1 x_0 (conj (proj1 hypx0) (Rge_le _ _ (Req_ge b x_0 stronghyp)))
                               (eps/2)%R hypeps2).
      destruct eta1proof as [eta1 goodeta1].
      assert (interval a' b' (x_0 - b  + a')%R).
      split.
      rewrite <- Rplus_0_l at 1.
      apply (Rplus_le_compat_r a').
      apply (Rplus_le_reg_r b).
      field_simplify.
      exact hyp.
      apply (Rplus_le_reg_r (b + - a')).
      field_simplify.
      unfold Rminus.
      rewrite Rplus_assoc.
      rewrite (Rplus_comm (-a')%R b').
      rewrite <- Rplus_assoc.
      exact (proj2 hypx0).
      pose (eta2proof := cont2 (x_0 - b + a')%R H (eps/2)%R hypeps2).
      destruct eta2proof as [eta2 goodeta2].
      pose (eta := Rmin eta1 eta2).
      exists eta.
      split.
      Search Rmin Rgt.
      apply Rmin_Rgt.
      exact (conj (proj1 goodeta1) (proj1 goodeta2)).
      intros x goodx.
      induction (Rle_dec x b) ; induction (Rle_dec x_0 b).
      apply (Rlt_trans _ (eps/2) eps).
      apply (proj2 goodeta1).
      split.
      split.
      split.
      exact (proj1 (proj1 (proj1 goodx))).
      exact a0.
      exact (proj2 (proj1 goodx)).
      apply (Rlt_le_trans _ eta _ ).
      exact (proj2 goodx).
      exact (Rmin_l _ _).
      apply Rminus_lt.
      apply (Rplus_lt_reg_l (eps / 2)).
      field_simplify.
      unfold Rdiv at 1.
      rewrite Rmult_0_l.
      unfold Rgt in hypeps2.
      exact hypeps2.
      apply (Rle_lt_trans _ 
                       (dist M (gamma1 x) (gamma1 b) + dist M (gamma1 b) (gamma2 (x_0 - b + a')%R))
                       _).
      Search Metric_Space.
      exact (dist_tri M (gamma1 x) (gamma2 (x_0 - b + a')%R) (gamma1 b)).
      Search Rplus Rlt.
      assert (dist M (gamma1 x) (gamma1 b) < eps/2)%R.
      rewrite stronghyp.
      apply ((proj2 goodeta1) x).
      split.
      split.
      exact (conj (proj1 (proj1 (proj1 goodx))) (a0)).
      exact (proj2 (proj1 goodx)).
      apply (Rlt_le_trans _ eta _).
      exact (proj2 goodx).
      exact (Rmin_l _ _).
      assert (dist M (gamma1 b) (gamma2 (x_0 - b + a')) < eps/2)%R.
      rewrite compatHyp. (* Enfin... *)
      rewrite stronghyp.
      unfold Rminus.
      Search Ropp Rplus.
      rewrite Rplus_opp_r.
      rewrite Rplus_0_l.
      Search Metric_Space.
      rewrite  (proj2 (dist_refl M (gamma2 a') (gamma2 a'))).
      unfold Rgt in hypeps2.
      exact hypeps2.
      reflexivity.
      Search Rlt Rplus.
      assert (eps = eps / 2 + eps / 2)%R.
      field.
      rewrite H2.
      apply (Rplus_lt_compat _ _ _ _ H0 H1).
      rewrite <-stronghyp.
      rewrite compatHyp.
      rewrite <- (Rplus_0_l a') at 2.
      Search Rminus 0%R.
      assert (x_0 = b) as stronghyprev.
      rewrite stronghyp.
      reflexivity.
      rewrite <- (Rminus_diag_eq _ _ stronghyprev).
      apply (Rlt_trans _ (eps/2)%R _).
      apply ((proj2 goodeta2) (x - b + a')%R).
      split.
      split.
      split.
      rewrite <- Rplus_0_l at 1.
      apply (Rplus_le_compat_r a').
      apply Rge_le.
      apply Rge_minus.
      exact (Rgt_ge _ _ (Rnot_le_gt _ _ b0)).
      apply (Rplus_le_reg_r (b - a')%R).
      field_simplify.
      unfold Rminus.
      rewrite Rplus_assoc.
      rewrite (Rplus_comm (-a')%R b').
      rewrite <- Rplus_assoc.
      exact (proj2 (proj1 (proj1 goodx))).
      intro.
      unfold Rminus in H0.
      apply (proj2 (proj1 goodx)).
      apply (Rplus_eq_compat_r (b - a')) in H0.
      field_simplify in H0.
      exact H0.
      unfold dist ; unfold R_met.
      unfold R_dist.
      assert (x - b + a' - (x_0 - b + a') = x - x_0)%R.
      field.
      rewrite H0.
      apply (Rlt_le_trans (Rabs (x - x_0)%R) eta eta2).
      exact (proj2 goodx).
      exact (Rmin_r eta1 eta2).
      apply (Rplus_lt_reg_l (-eps / 2)).
      field_simplify.
      unfold Rdiv at 1.
      rewrite Rmult_0_l.
      unfold Rgt in hypeps2.
      exact hypeps2.
      apply (Rlt_trans _ (eps/2)%R _).
      apply (proj2 goodeta2).
      split.
      split.
      split.
      rewrite <- Rplus_0_l at 1.
      apply (Rplus_le_compat_r a'). 
      apply Rge_le.
      apply Rge_minus.
      apply (Rnot_le_gt) in b0.
      exact (Rgt_ge _ _ b0).
      apply (Rplus_le_reg_r (b - a')%R).
      field_simplify.
      unfold Rminus.
      rewrite Rplus_assoc.
      rewrite (Rplus_comm (-a')%R b').
      rewrite <- Rplus_assoc.
      exact (proj2 (proj1 (proj1 goodx))).
      intro.
      apply (Rplus_eq_compat_r (b - a')) in H0.
      field_simplify in H0.
      exact ((proj2 (proj1 goodx)) H0).
      unfold dist ; unfold R_met.
      unfold R_dist.
      assert (x - b + a' - (x_0 - b + a') = x - x_0)%R.
      field.
      rewrite H0.
      apply (Rlt_le_trans (Rabs (x - x_0)%R) eta eta2).
      exact (proj2 goodx).
      exact (Rmin_r eta1 eta2).
      apply (Rplus_lt_reg_l (-eps / 2)).
      field_simplify.
      unfold Rdiv at 1.
      rewrite Rmult_0_l.
      unfold Rgt in hypeps2.
      exact hypeps2.
  Qed.


  Definition pconcat {a b a' b' : R} (g1 : chemin a b)
             (g2 : chemin a' b') (p : compat g1 g2) : chemin a (b + b' - a') :=
    exist (fun g => continue_in R_met M  g (interval a (b + b' - a')%R))
          (concat g1 g2) (concat_is_continuous a b a' b' g1 g2 p).


    (* Dérivabilité des chemins, et en particulier, chemins C1 par bouts *)
  Proposition sous_chemin_continu : forall {a b : R} (gamma : chemin a b) (m1 m2 : R),
      (interval a b m1) -> (interval a b m2) ->
      continue_in R_met M  (proj1_sig gamma) (interval m1 m2).
  Proof.
    intros.
    apply (subset_continuity (proj1_sig gamma) (interval m1 m2) (interval a b)).
    exact (proj2_sig gamma).
    intros x hyp.
    split.
    exact (Rle_trans a m1 x (proj1 H) (proj1 hyp)).
    exact (Rle_trans x m2 b (proj2 hyp) (proj2 H0)).
  Qed.

  Proposition sous_chemin {a b : R} (gamma : chemin a b) (m1 m2 : R) (p1 : interval a b m1)
             (p2 : interval a b m2) : chemin m1 m2.
  Proof.
    exists (proj1_sig gamma).
    exact (sous_chemin_continu gamma m1 m2 p1 p2).
  Qed.
  Print sous_chemin.

  Inductive C1_par_bouts (a b : R) (p : (a <= b)%R) (gamma : chemin a b) :=
  |Basic : RCC1_in (finterval a b) (proj1_sig gamma) -> C1_par_bouts a b p gamma
  |Concat (m : R) (pr : interval a b m) :
     RCC1_in (finterval a m) (proj1_sig (sous_chemin gamma a m (interval_deb a b p) pr)) ->
     C1_par_bouts m b (proj2 pr) (sous_chemin gamma m b pr (interval_fin a b p)) ->
     C1_par_bouts a b p gamma.




End Chemins.
