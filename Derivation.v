Load Continuity.
(* On reprend les définitions de Ranalysis, mais on rajoute une notion de dérivée
     dans un ouvert : derivable_in. Si l'on avait défini cela dans R, on aurait
     pu obtenir
        derivable_in ]-inf,0[ Rabs, et derive_pt_in ]-1,0[ Rabs 0 = -1 *)

(* Ajouts à la dérivation réelle *)

Definition R_differentiate (f : R -> R) (x0 x : R) :=
  ((f x - f x0)/(x - x0))%R.

Definition derivable_pt_lim_in (U : R -> Prop) (f : R -> R) (x l : R) :=
  limit_in R_met R_met (R_differentiate f x) (D_x R U x)  x l.
Definition derivable_pt_in (U : R -> Prop) (f : R -> R) (x : R) :=
  {l : R | derivable_pt_lim_in U f x l}.
Definition derive_pt_in {U : R -> Prop} {f : R -> R} {x : R} (p : derivable_pt_in U f x) :=
  proj1_sig p.

(* Dérivation de R dans C *)

Definition RC_differentiate (f : R -> C) (x0 : R) :=
  (fun x => (f x - f x0)/ (R_to_C x - R_to_C x0)).

Definition RC_derivable_pt_in (U : R -> Prop) (f : R -> C) (x0 : R) :=
  {z : C | limit_in R_met C_met (RC_differentiate f x0) (D_x R U x0) x0 z}.

Definition RC_derivable_pt (f : R -> C) (x0 : R) :=
  {z : C | limit_in R_met C_met (RC_differentiate f x0) (D_x R no_cond x0) x0 z}.

Definition RC_derivable_in (U : R -> Prop) (f : R -> C) :=
  forall x : R, U x -> RC_derivable_pt_in U f x.

Definition RC_derivable (f : R -> C) :=
  forall x : R, RC_derivable_pt f x.

Definition RC_derive_pt_in {U : R -> Prop} {f : R -> C} {x : R}
           (pr : RC_derivable_pt_in U f x) :=
  proj1_sig pr.

Definition RC_derive_pt {f : R -> C} {x : R} (pr : RC_derivable_pt f x) :=
  proj1_sig pr.

Definition RC_derive_in {U : R -> Prop} {f : R -> C} (pr : RC_derivable_in U f)
           (c : {x : R | U x}) := RC_derive_pt_in (pr (proj1_sig c) (proj2_sig c)).

Definition RC_derive_in_abs (U : R -> Prop) (f : R -> C) (f' : R -> C) :=
  forall x0 : R, U x0 -> limit_in R_met C_met (RC_differentiate f x0) (fun x => True) x0 (f' x0).

Definition RC_derive (f : R -> C) (pr : RC_derivable f) (x : R) :=
  RC_derive_pt (pr x).

Definition RCC1_in (D : R -> Prop) (f : R -> C) :=
  { f' : R -> C | ((continue_in R_met C_met f' D) /\ RC_derive_in_abs D f' f)}.
Search "continue".
Definition RCC1 (f : R -> C) :=
  {pr : RC_derivable f | continue R_met C_met (RC_derive f pr)}.



(* Dérivation de C dans C *)

Definition C_differentiate (f : C -> C) (z0 : C) :=
  (fun z => ((f z - f z0)/ z - z0)).

Definition C_derivable_pt_in (U : C -> Prop) (f : C -> C) (z0 : C) :=
  {z : C | limit_in C_met C_met (C_differentiate f z0) (D_x C U z0) z0 z}.

Definition C_derivable_pt (f : C -> C) (z0 : C) :=
  {z : C | limit_in C_met C_met (C_differentiate f z0) (D_x C (fun x => True) z0) z0 z}.

Definition C_derivable_in (U : C -> Prop) (f : C -> C) :=
  forall z : C, U z -> C_derivable_pt_in U f z.

Definition C_derivable (f : C -> C) :=
  forall z : C, C_derivable_pt f z.

Definition C_derive_pt_in (U : C -> Prop) (f : C -> C) (z : C)
           (pr : C_derivable_pt_in U f z) :=
  proj1_sig pr.

Definition C_derive_pt (f : C -> C) (z : C) (pr : C_derivable_pt f z) :=
  proj1_sig pr.

Definition C_derive_in (U : C -> Prop) (f : C -> C) (pr : C_derivable_in U f)
           (c : {x : C | U x}) := C_derive_pt_in U f (proj1_sig c) (pr (proj1_sig c) (proj2_sig c)).

Definition C_derive (f : C -> C) (pr : C_derivable f) (z : C) :=
  C_derive_pt f z (pr z).

(* Adéquation dérivation RC et dérivation R *)

Proposition diff_is_good_Re : forall (f : R -> C) (x : R) (y : R),
    x <> y -> Re (RC_differentiate f x y) = R_differentiate (Re_func f) x y.
Proof.
  intros.
  unfold RC_differentiate ; unfold R_differentiate ; unfold Re_func.
  unfold comp ; unfold Cminus ; unfold Rminus.
  assert (R_to_C y + - R_to_C x = R_to_C (y + - x)%R).
  apply CInj.
  rewrite Cplus_def_Im.
  rewrite Copp_def_Im.
  repeat (rewrite RtoC_noIm).
  field.
  rewrite Cplus_def_Re ; rewrite Copp_def_Re.
  repeat (rewrite RInj).
  field.
  rewrite H0.
  unfold Cdiv.
  rewrite Cinv_RtoC.
  repeat (rewrite Cmult_def_Re).
  repeat (rewrite Cplus_def_Re).
  rewrite Copp_def_Re.
  repeat (rewrite RInj).
  repeat (rewrite RtoC_noIm).
  field.
  Search R not eq.
  apply Rminus_eq_contra.
  Search not eq.
  apply not_eq_sym.
  exact H.
Qed.
Proposition diff_is_good_Im : forall (f : R -> C) (x : R) (y : R),
    x <> y -> Im (RC_differentiate f x y) = R_differentiate (Im_func f) x y.
Proof.
  intros.
  unfold RC_differentiate ; unfold R_differentiate ; unfold Im_func.
  unfold comp ; unfold Cminus ; unfold Rminus.
  assert (R_to_C y + - R_to_C x = R_to_C (y + - x)%R).
  apply CInj.
  rewrite Cplus_def_Im.
  rewrite Copp_def_Im.
  repeat (rewrite RtoC_noIm).
  field.
  rewrite Cplus_def_Re ; rewrite Copp_def_Re.
  repeat (rewrite RInj).
  field.
  rewrite H0.
  unfold Cdiv.
  repeat (rewrite Cmult_def_Im) ; repeat (rewrite Cplus_def_Im).
  repeat (rewrite Cinv_RtoC).
  repeat (rewrite Cplus_def_Re).
  rewrite Copp_def_Re ; rewrite Copp_def_Im.
  repeat (rewrite RInj).
  repeat (rewrite RtoC_noIm).
  field.
  apply Rminus_eq_contra ; apply not_eq_sym.
  exact H.
Qed.
Print derivable_pt_abs.
Print derivable_pt_lim.
Check limit1_in.


Print D_x.
Print derivable_pt.
Print limit_in.


Proposition derivable_pt_in_is_good_Re : forall (U : R -> Prop) (f : R -> C) (x : R),
    RC_derivable_pt_in U f x -> derivable_pt_in U (Re_func f) x.
Proof.
  intros.
  unfold derivable_pt_in.
  destruct H as [l limit].
  exists (Re l).
  unfold derivable_pt_lim_in.
  unfold limit_in in *.
  intros eps hypeps.
  pose (etaproof := limit eps hypeps).
  destruct etaproof as [eta [etagt0 etagood]].
  exists eta.
  split.
  exact etagt0.
  intros x0 Hx0.
  Search R_differentiate.
  rewrite<- diff_is_good_Re.
  simpl in *.
  apply (Rle_lt_trans _ _ eps (Cdist_good_Re _ l) (etagood x0 Hx0)).
  exact (proj2 (proj1 Hx0)).
Qed.

Proposition derivable_pt_in_is_good_Im : forall (U : R -> Prop) (f : R -> C) (x : R),
    RC_derivable_pt_in U f x -> derivable_pt_in U (Im_func f) x.
Proof.
  intros.
  unfold derivable_pt_in.
  destruct H as [l limit].
  exists (Im l).
  unfold derivable_pt_lim_in.
  unfold limit_in in *.
  intros eps hypeps.
  pose (etaproof := limit eps hypeps).
  destruct etaproof as [eta [etagt0 etagood]].
  exists eta.
  split.
  exact etagt0.
  intros x0 Hx0.
  rewrite<- diff_is_good_Im.
  simpl in *.
  apply (Rle_lt_trans _ _ eps (Cdist_good_Im _ l) (etagood x0 Hx0)).
  exact (proj2 (proj1 Hx0)).
Qed.


Proposition derivable_pt_is_good_Re : forall (f : R -> C) (x : R),
    RC_derivable_pt f x -> derivable_pt (Re_func f) x.
Proof.
  intros.
  unfold derivable_pt.
  unfold RC_derivable_pt in H.
  destruct H.
  exists (Re x0).
  unfold derivable_pt_abs.
  unfold derivable_pt_lim.
  intros eps hypeps.
  unfold limit_in in l.
  pose (etaproof := l eps hypeps).
  destruct etaproof as [eta [etagt0 etagood]].
  Print posreal.
  exists (mkposreal eta etagt0).
  intros h hnot0 hyph.
  unfold Re_func ; unfold comp.
  assert (R_dist (x + h)%R x < eta)%R.
  unfold R_dist.
  unfold Rminus ; rewrite Rplus_assoc ; rewrite (Rplus_comm h (- x)) .
  rewrite <- Rplus_assoc ; rewrite Rplus_opp_r ; rewrite Rplus_0_l.
  exact hyph.
  assert (x <> x + h)%R.
  intro.
  apply hnot0.
  Search Rplus eq.
  apply (Rplus_eq_compat_l (-x)) in H0.
  field_simplify in H0.
  rewrite H0 ; reflexivity.
  pose (good := etagood (x + h)%R (conj (conj I H0) H)).
  simpl in good.
  unfold RC_differentiate in good.
  unfold Cdist in good.
  rewrite CNorm2_def in good.
  unfold dist_euc in good.
  apply (fun p => Rle_lt_trans _ _ _ p good).
  Search sqrt.
  rewrite <- sqrt_Rsqr_abs.
  apply sqrt_le_1_alt.
  rewrite<- Rplus_0_r at 1.
  apply Rplus_le_compat.
  unfold Cminus.
  unfold Cdiv.
  repeat (rewrite Cplus_def_Re).
  repeat (rewrite Copp_def_Re).
  repeat (rewrite Cmult_def_Re).
  assert (R_to_C (x + h) + - R_to_C x = R_to_C h).
  apply CInj.
  rewrite Cplus_def_Im.
  rewrite Copp_def_Im.
  repeat (rewrite RtoC_noIm).
  field.
  rewrite Cplus_def_Re ; rewrite Copp_def_Re ; repeat (rewrite RInj).
  field.
  rewrite H1.
  repeat (rewrite Cinv_RtoC).
  repeat (rewrite RInj).
  repeat (rewrite RtoC_noIm).
  repeat (rewrite Cplus_def_Re).
  repeat (rewrite Cplus_def_Im).
  rewrite Copp_def_Re.
  rewrite Rmult_0_r.
  rewrite Rminus_0_r.
  unfold Rdiv.
  rewrite Rminus_0_l.
  rewrite Rsqr_neg.
  right.
  reflexivity.
  Search Rsqr.
  apply Rle_0_sqr.
Qed.

Proposition derivable_pt_is_good_Im : forall (f : R -> C) (x : R),
    RC_derivable_pt f x -> derivable_pt (Im_func f) x.
Proof.
  intros.
  unfold RC_derivable_pt.
  unfold RC_derivable_pt in H.
  destruct H.
  exists (Im x0).
  unfold derivable_pt_abs.
  unfold derivable_pt_lim.
  intros eps hypeps.
  unfold limit_in in l.
  pose (etaproof := l eps hypeps).
  destruct etaproof as [eta [etagt0 etagood]].
  Print posreal.
  exists (mkposreal eta etagt0).
  intros h hnot0 hyph.
  unfold Re_func ; unfold comp.
  assert (R_dist (x + h)%R x < eta)%R.
  unfold R_dist.
  unfold Rminus ; rewrite Rplus_assoc ; rewrite (Rplus_comm h (- x)) .
  rewrite <- Rplus_assoc ; rewrite Rplus_opp_r ; rewrite Rplus_0_l.
  exact hyph.
  assert (x <> x + h)%R.
  intro.
  apply hnot0.
  Search Rplus eq.
  apply (Rplus_eq_compat_l (-x)) in H0.
  field_simplify in H0.
  rewrite H0 ; reflexivity.
  pose (good := etagood (x + h)%R (conj (conj I H0) H)).
  simpl in good.
  unfold RC_differentiate in good.
  unfold Cdist in good.
  rewrite CNorm2_def in good.
  unfold dist_euc in good.
  apply (fun p => Rle_lt_trans _ _ _ p good).
  Search sqrt.
  rewrite <- sqrt_Rsqr_abs.
  apply sqrt_le_1_alt.
  rewrite<- Rplus_0_l at 1.
  apply Rplus_le_compat.
  apply Rle_0_sqr.
  unfold Cminus.
  unfold Cdiv.
  repeat (rewrite Cplus_def_Im).
  repeat (rewrite Copp_def_Im).
  repeat (rewrite Cmult_def_Im).
  assert (R_to_C (x + h) + - R_to_C x = R_to_C h).
  apply CInj.
  rewrite Cplus_def_Im.
  rewrite Copp_def_Im.
  repeat (rewrite RtoC_noIm).
  field.
  rewrite Cplus_def_Re ; rewrite Copp_def_Re ; repeat (rewrite RInj ).
  field.
  rewrite H1.
  repeat (rewrite Cinv_RtoC).
  repeat (rewrite RInj).
  repeat (rewrite RtoC_noIm).
  repeat (rewrite Cplus_def_Re).
  repeat (rewrite Cplus_def_Im).
  rewrite Copp_def_Im.
  rewrite Rmult_0_l.
  rewrite Rplus_0_r.
  unfold Rdiv.
  rewrite Rminus_0_l.
  rewrite Rsqr_neg.
  right.
  reflexivity.
Qed.
Print RCC1.
Print derivable.
Print RC_derivable_in.
Definition derivable_in (U : R -> Prop) (f : R -> R) :=
  forall x : R, U x -> derivable_pt_in U f x.

Proposition derivable_is_good_in_Re : forall (U : R -> Prop) (f : R -> C),
    RC_derivable_in U f -> derivable_in U (Re_func f).
Proof.
  intros.
  unfold derivable_in.
  unfold RC_derivable_in in H.
  intros x Ux.
  apply derivable_pt_in_is_good_Re.
  apply H.
  exact Ux.
Qed.
Proposition derivable_is_good_in_Im : forall (U : R -> Prop) (f : R -> C),
    RC_derivable_in U f -> derivable_in U (Im_func f).
Proof.
  intros.
  unfold RC_derivable_in in H.
  intros x Ux.
  apply derivable_pt_in_is_good_Im.
  apply H. exact Ux.
Qed.

Proposition derivable_is_good_Re : forall (f : R -> C),
    RC_derivable f -> derivable (Re_func f).
Proof.
  intros.
  unfold derivable ; unfold RC_derivable in H.
  intro x.
  apply derivable_pt_is_good_Re.
  exact (H x).
Qed.
Proposition derivable_is_good_Im : forall (f : R -> C),
    RC_derivable f -> derivable (Im_func f).
Proof.
  intros.
  unfold derivable ; unfold RC_derivable in H.
  intro x.
  apply derivable_pt_is_good_Im.
  exact (H x).
Qed.
Print limit_in.
(*Proposition unicity_limit {E F : Metric_Space} :
  forall (f g : Base E -> Base F) (x : Base E) (U : Base E -> Prop) (l1 l2 : Base F),
           (forall y : Base E, U x -> f y = g y) -> (limit_in E F f U x l1) ->
           (limit_in E F g U x l2) -> l1 = l2. 
Proof.
  intros.
  Search dist.
  apply (proj1 (dist_refl F l1 l2)).
  apply cond_eq.
  intros eps hypeps.
  pose (eps2 := (eps / 2)%R).
  assert (0 < eps2)%R.
  unfold eps2.
  apply (Rmult_lt_reg_l 2).
  prove_sup.
  field_simplify.
  exact hypeps.
  pose (eta1proof := H0 eps2 H2).
  destruct eta1proof as [eta1 [hypeta1 goodeta1]].
  pose (eta2proof := H1 eps2 H2).
  destruct eta2proof as [eta2 [hypeta2 goodeta2]].
  pose (eta := Rmin eta1 eta2).
  assert 
(* Après la dérivabilité, la dérivée : *)
Check RC_derive_pt_in.
Proposition derive_pt_in_good_Re : forall (U : R -> Prop) (f : R -> C) (x : R)
                                          (pr : RC_derivable_pt_in U f x)
                                          (prRe : derivable_pt_in U (Re_func f) x),
    Re (RC_derive_pt_in pr) = derive_pt_in prRe.

Proof.
  intros.
  destruct pr as [z0 limitz0]; destruct prRe as [x0 limitx0].
  simpl in *.
  unfold derivable_pt_lim_in in limitx0.
  Search limit_in
  unfold limit_in in *.
  apply cond_eq.
  intros eps hypeps.
  fold (R_dist (Re z0) x0).
  pose (Cgood := limitz0 eps hypeps).
  destruct Cgood as [etaC [hypetaC etaCgood]].
  pose (Rgood := limitx0 eps hypeps).
  destruct Rgood as [etaR [hypetaR etaRgood]].
  Sea

Print RCC1_in.
Definition RCC1_good_Re (f : R -> C) (p : RCC1 f) := 
  mkC1 (Re_func f) (derivable_is_good_Re f
*)



(* Petits résultats pour les chemins *)

Proposition subset_C1 : forall (E F : R -> Prop) (f : R -> C),
    (RCC1_in F f) -> (subset E F) -> (RCC1_in E f).
Proof.
  unfold subset.
  intros E F f [f' [contprf' derf'f]] hyp.
  unfold RCC1_in.
  exists f'.
  split.
  apply (subset_continuity f' E F contprf' hyp).
  unfold RC_derive_in_abs.
  intros x0 Ex0.
  unfold limit_in.
  intros eps hypeps.
  pose (etaproof := derf'f x0 (hyp x0 Ex0) eps hypeps).
  destruct etaproof as [eta [etagt0 etagood]].
  exists eta.
  split.
  exact etagt0.
  intros x [_ dxx0].
  exact (etagood x (conj I dxx0)).
Qed.

Definition finterval (a b : R) := (fun x => (a < x < b)%R).
