Load Cdefs.

Import CDef.
Open Scope C_scope.

Inductive pol : Type :=
|Pconst : C -> pol
|PX: pol -> C -> pol.

Definition Px (p:pol) (a:C): pol :=
  match p with 
  | Pconst C0 => Pconst a
  | _ => PX p a
  end. 

Fixpoint plusPol ( p q : pol): pol  :=
match p , q with
  |Pconst a, Pconst b => Pconst (a + b)
  |Pconst a, PX q1 b => PX q1 (a+b)
  |PX p1 a, Pconst b => PX p1 (a+b)
  |PX p1 a , PX q1 b => Px (plusPol p1 q1)(a+b)
  end.

Inductive nulP: pol -> Prop:=
  |nulPconst : forall a, a= C0 -> nulP (Pconst a)
  |nulPX: forall p a, nulP p -> a= C0 -> nulP (PX p a).

Inductive equalP : pol-> pol -> Prop := 
  |equalConst: forall a b, a = b -> equalP (Pconst a) (Pconst b)
  |equalPX : forall p q a b, equalP p q -> a = b -> equalP (PX p a) (PX q b)
  |equalnul: forall p a, nulP p -> equalP (PX p a) (Pconst a)
  |equalnulrev: forall p a, nulP p -> equalP (Pconst a) (PX p a).

Lemma equalP_ref: forall p, equalP p p.
Proof.
  intros. induction p. apply equalConst. reflexivity.
  apply equalPX. apply IHp. reflexivity.
Qed.

Lemma equalP_sym : forall p q, equalP p q -> equalP q p.
Proof.
  induction p; induction q. intros. apply equalConst. inversion H. rewrite H2. reflexivity.
  intros. inversion H. apply equalnul. apply H2.
  intros. inversion H. apply equalnulrev. apply H1.
  intros. inversion H. apply equalPX. apply IHp. apply H3. rewrite H5. reflexivity.
Qed.

Lemma nul_equal : forall p q, nulP p -> nulP q -> equalP p q.
Proof.
  induction p. induction q. intros. apply equalConst. inversion H.
  inversion H0. rewrite H2; rewrite H4; reflexivity.
  intros. inversion H. inversion H0. rewrite <- H3. rewrite <- H2 in H6.
  rewrite H6. apply equalnulrev. rewrite <- H3 in H5. apply H5.
  intros. inversion H. inversion H0. rewrite <- H4 in H5.
  rewrite H5. apply equalnul. apply H3.
  inversion H. apply equalPX. apply IHp. apply H10. apply H5. rewrite H6 ; apply H11.
Qed.

Lemma nulP_comp : forall p q, nulP p -> equalP p q -> nulP q.
Proof.
  induction p; induction q. intros. inversion H0. rewrite <- H3. apply H.
  intros. inversion H. inversion H0.  apply nulPX. apply H5. rewrite <-H6. apply H2.
  intros. inversion H. inversion H0. apply nulPconst. rewrite <-H8; apply H4.
  intros. inversion H. inversion H0. apply nulPX. apply IHp. apply H3. apply H8. 
  rewrite <- H10. apply H4.
Qed.

Lemma equalP_trans : forall p q r, equalP p q -> equalP q r -> equalP p r.
Proof.
  induction p , q. intros. inversion H. rewrite H3. apply H0.
  intros. inversion H. inversion H0. rewrite <- H9. apply equalnulrev. 
  destruct H8. apply nulP_comp with q.
  apply H3. apply H7. apply equalP_ref.
  intros. inversion H. inversion H0. rewrite <- H6. 
  apply equalnul. apply H2. apply equalPX. apply nul_equal. apply H2. apply H6. reflexivity.
  intros. inversion H. inversion H0. apply equalPX. apply IHp with q. apply H4. 
  apply H9. rewrite <-H11. apply H6.
  rewrite H6. apply equalnul. apply equalP_sym in H4. apply nulP_comp with q. 
  apply H10. apply H4.
Qed.

Lemma commPlus : forall (p q: pol), (plusPol p q) = (plusPol q p).
Proof.
  induction p,q. 
  intros. 
  * simpl. 
    rewrite Cplus_comm.
    reflexivity.
  * simpl ; rewrite Cplus_comm ; reflexivity.
  * simpl ; rewrite Cplus_comm ; reflexivity.
  * simpl ;
    rewrite Cplus_comm.
    rewrite IHp.
    reflexivity.
Qed.

Fixpoint evalP (p:pol) (z:C) : C :=
  match p with
  |Pconst a => a
  |PX q b => z* (evalP q z)+ b
  end.

Fixpoint derivP (p:pol): pol :=
match p with
  |Pconst a => Pconst C0
  |PX q b => plusPol q (PX (derivP q) C0)
  end.

Fixpoint cons_mul_Pol (c : C) (p : pol) := 
  match p with
  |Pconst a => Pconst (a * c)
  |PX p a => PX (cons_mul_Pol c p) (a * c)
  end.
Fixpoint mulPol ( p q : pol) := 
  match p with
  |Pconst a => cons_mul_Pol a q
  |PX p a => plusPol (cons_mul_Pol a q) (mulPol p q)
  end.

